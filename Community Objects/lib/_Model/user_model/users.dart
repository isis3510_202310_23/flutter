import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../services/user_service.dart';

class UserFacade {
  User? _currentUser;
  DatabaseReference _databaseReference;

  UserFacade()
      : _currentUser = FirebaseAuth.instance.currentUser,
        _databaseReference = FirebaseDatabase.instance.ref();

  static Future<void> editProfile() async {
    // Implementa la funcionalidad de edición de perfil aquí
  }

  Future<void> logout() async {
    await FirebaseAuth.instance.signOut();
  }

  Future<String> getEmail() async {
    String email = FirebaseAuth.instance.currentUser!.email ?? '';
    return email;
  }

  Future<String> getUsername() async {
    String email = FirebaseAuth.instance.currentUser!.email!;
    final userServices = UserServices();
    final userData = await userServices.getUserDataByEmail(email);

    if (userData != null) {
      return userData['username'] ?? '';
    } else {
      print('No hay datos disponibles.');
      return '';
    }
  }

  Future<String> getName() async {
    String email = FirebaseAuth.instance.currentUser!.email!;
    final userServices = UserServices();
    final userData = await userServices.getUserDataByEmail(email);

    if (userData != null) {
      return userData['name'] ?? '';
    } else {
      print('No hay datos disponibles.');
      return '';
    }
  }

    Future<String> getGender() async {
    String email = FirebaseAuth.instance.currentUser!.email!;
    final userServices = UserServices();
    final userData = await userServices.getUserDataByEmail(email);

    if (userData != null) {
      return userData['gender'] ?? '';
    } else {
      print('No hay datos disponibles.');
      return '';
    }
  }

    Future<String> getCareer() async {
    String email = FirebaseAuth.instance.currentUser!.email!;
    final userServices = UserServices();
    final userData = await userServices.getUserDataByEmail(email);

    if (userData != null) {
      return userData['career'] ?? '';
    } else {
      print('No hay datos disponibles.');
      return '';
    }
  }

    Future<String> getDonations() async {
    String email = FirebaseAuth.instance.currentUser!.email!;
    final userServices = UserServices();
    final userData = await userServices.getUserDataByEmail(email);

    if (userData != null) {
      return userData['donations'] ?? '';
    } else {
      print('No hay datos disponibles.');
      return '';
    }
  }


  Future<String> getAge() async {
    String email = FirebaseAuth.instance.currentUser!.email!;
    final userServices = UserServices();
    final userData = await userServices.getUserDataByEmail(email);

    if (userData != null) {
      return userData['age'] ?? '';
    } else {
      print('No hay datos disponibles.');
      return '';
    }
  }

  Future<String> getImagePath() async {
    return 'lib/images/avatarPPic.png';
  }
}
