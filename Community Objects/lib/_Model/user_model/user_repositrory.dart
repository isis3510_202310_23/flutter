import '../../_Model/user_model/users.dart';
import '../services/user_service.dart';

abstract class UserRepository {
  Future<String> resetPassword(String email);
  Future<String> loginUser(String username, String password);
  Future<String> registerUser(String name, String gender, String age,
      String username, String email, String password);
  Future<Map<String, dynamic>?> getUserDataByEmail(String email);
}

class UserRepositoryImpl implements UserRepository {
  final UserServices _userServices;

  UserRepositoryImpl() : _userServices = UserServices();

  @override
  Future<String> resetPassword(String email) async {
    return await _userServices.resetPassword(email);
  }

  @override
  Future<String> loginUser(String username, String password) async {
    return await _userServices.loginUser(username, password);
  }

  @override
  Future<String> registerUser(String name, String gender, String age,
      String username, String email, String password) async {
    return await _userServices.registerUser(
        name, gender, age, username, email, password);
  }

  @override
  Future<Map<String, dynamic>?> getUserDataByEmail(String email) async {
    return await _userServices.getUserDataByEmail(email);
  }
}
