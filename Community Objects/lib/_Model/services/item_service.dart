import 'package:cloud_firestore/cloud_firestore.dart';

import '../models/item.dart';

class ItemServices {
  Future<DocumentSnapshot> getCategoryPopularities() async {
    try {
      final snapshot = await FirebaseFirestore.instance
          .collection('category_popularity')
          .doc("category_popularity")
          .get();
      return snapshot;
    } catch (e) {
      return Future.error(e);
    }
  }

  Future<void> updateCategoryPopularity(String categoryName) async {
    try {
      await FirebaseFirestore.instance
          .collection('category_popularity')
          .doc("category_popularity")
          .update({categoryName: FieldValue.increment(1)});
    } catch (e) {
      return Future.error(e);
    }
  }

  Future<QuerySnapshot> getItemsFromCategory(String categoryName) async {
    try {
      final snapshot = FirebaseFirestore.instance
          .collection(categoryName)
          .where('category', isEqualTo: categoryName)
          .limit(5)
          .get();
      return snapshot;
    } catch (e) {
      return Future.error(e);
    }
  }

  Future<DocumentSnapshot> getItem(String id, String categoryName) async {
    try {
      final snapshot = await FirebaseFirestore.instance
          .collection(categoryName)
          .doc(id)
          .get();
      return snapshot;
    } catch (e) {
      return Future.error(e);
    }
  }

  Future<List<Item>> getItems(String query) async {
    try {
      List<Item> result = [];
      result = await searchCategory(query, "Clothes", result);
      result = await searchCategory(query, "Equipment", result);
      result = await searchCategory(query, "Printed", result);
      result = await searchCategory(query, "Supplies", result);

      return result;
    } catch (e) {
      return Future.error(e);
    }
  }

  Future<List<Item>> searchCategory(
      String query, String categoryName, List<Item> inList) async {
    String whereClause = "name";
    if (categoryName == "Printed" || categoryName == "Supplies") {
      whereClause = "title";
    }

    final snapshot = await FirebaseFirestore.instance
        .collection(categoryName)
        .where(whereClause, isGreaterThanOrEqualTo: query)
        .where(whereClause, isLessThanOrEqualTo: "$query\uf7ff")
        .get();
    for (var element in snapshot.docs) {
      Item i = Item.createFromJson(element.data() as Map);
      i.setId(element.id);
      inList.add(i);
    }
    return inList;
  }
}
