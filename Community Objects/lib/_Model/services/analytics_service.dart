import 'package:firebase_analytics/firebase_analytics.dart';

class AnalyticsService {
  final FirebaseAnalytics _analytics = FirebaseAnalytics.instance;

  FirebaseAnalyticsObserver getAnalyticsObserver() =>
      FirebaseAnalyticsObserver(analytics: _analytics);

  Future logSignUp() async {
    await _analytics.logSignUp(signUpMethod: 'email');
  }

  Future logFunctionalityUse(String functionalityName) async {
    await _analytics.logEvent(
      name: "functionality_used",
      parameters: {
        "value": functionalityName,
      },
    );
  }
}
