import 'package:cloud_firestore/cloud_firestore.dart';

import '../models/item.dart';

class SearchServices {
  Future<void> updateSearchTerm(String query) async {
    try {
      await FirebaseFirestore.instance
          .collection('searches')
          .doc(query)
          .set({"count": FieldValue.increment(1)});
    } catch (e) {
      return Future.error(e);
    }
  }
}
