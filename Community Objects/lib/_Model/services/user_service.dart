import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

class UserServices {
  Future<String> resetPassword(String email) async {
    try {
      await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
      return "Success";
    } on FirebaseAuthException catch (error) {
      return getError(error.code);
    } catch (e) {
      return e.toString();
    }
  }

  Future<String> loginUser(String username, String password) async {
    try {
      final snapshot = await FirebaseDatabase.instance
          .ref()
          .child('users/' + username)
          .get();
      if (snapshot.exists) {
        Map user = snapshot.value as Map;

        await FirebaseAuth.instance.signInWithEmailAndPassword(
            email: user['email'], password: user['password']);
        return "Success";
      } else {
        return "The user was not found.";
      }
    } on FirebaseAuthException catch (error) {
      return getError(error.code);
    } catch (error) {
      return error.toString();
    }
  }

  Future<String> registerUser(String name, String gender, String age,
      String username, String email, String password) async {
    try {
      await FirebaseDatabase.instance.ref().child('users/' + username).set({
        'age': age,
        'email': email,
        'gender': gender.toLowerCase(),
        'name': name,
        'password': password,
        'username': username,
      });

      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      return "Success";
    } on FirebaseAuthException catch (error) {
      return getError(error.code);
    } catch (e) {
      return e.toString();
    }
  }

  Future<Map<String, dynamic>?> getUserDataByEmail(String email) async {
    final ref = FirebaseDatabase.instance.ref();
    final snapshot =
        await ref.child('users').orderByChild('email').equalTo(email).once();

    if (snapshot.snapshot.value != null) {
      Map<dynamic, dynamic> data =
          snapshot.snapshot.value as Map<dynamic, dynamic>;
      Map<String, dynamic> userData =
          Map<String, dynamic>.from(data.values.first);
      return userData;
    } else {
      print('No hay datos disponibles.');
      return null;
    }
  }

  String getError(String errorCode) {
    switch (errorCode) {
      case "ERROR_EMAIL_ALREADY_IN_USE":
      case "account-exists-with-different-credential":
      case "email-already-in-use":
        return "Email already used. Go to login page.";
      case "ERROR_WRONG_PASSWORD":
      case "wrong-password":
        return "Wrong email/password combination.";
      case "ERROR_USER_NOT_FOUND":
      case "user-not-found":
        return "No user found with this email.";
      case "ERROR_USER_DISABLED":
      case "user-disabled":
        return "User disabled.";
      case "ERROR_TOO_MANY_REQUESTS":
      case "operation-not-allowed":
        return "Too many requests to log into this account.";
      case "ERROR_OPERATION_NOT_ALLOWED":
      case "operation-not-allowed":
        return "Server error, please try again later.";
      case "ERROR_INVALID_EMAIL":
      case "invalid-email":
        return "Email address is invalid.";
      default:
        return "Login failed. Please try again.";
    }
  }
}
