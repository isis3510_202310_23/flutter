import 'package:flutter/material.dart';

class Categoria {
  final String id;
  final String nombre;
  final String imagen;
  final String dbName;
  int totalSeleccionado;

  Categoria(
      {required this.id,
      required this.nombre,
      required this.imagen,
      required this.dbName,
      required this.totalSeleccionado});

  void incrementarTotalSeleccionado() {
    totalSeleccionado++;
  }
}
