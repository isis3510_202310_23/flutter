import 'item.dart';

class Supplies extends Item {
  String reference;

  Supplies(
      {required this.reference,
      // Super
      required super.name,
      required super.category,
      required super.imageURL,
      required super.description,
      required super.user});

  @override
  Map separateExtras() {
    return {
      "Reference": reference,
    };
  }

  factory Supplies.createFromJson(Map json) {
    return Supplies(
        reference: json['reference'],
        // Super
        name: json['title'],
        category: json['category'],
        imageURL: json['imageURL'],
        description: json['description'],
        user: json['user']);
  }
}
