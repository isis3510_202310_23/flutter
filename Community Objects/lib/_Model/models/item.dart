import 'dart:math';

import 'supplies.dart';
import 'equipment.dart';
import 'prints.dart';
import 'clothes.dart';

class Item {
  String name;
  String category;
  String imageURL;
  String description;
  String user;

  late String id;

  Item(
      {required this.name,
      required this.category,
      required this.imageURL,
      required this.description,
      required this.user});

  void setId(String pId) {
    id = pId;
  }

  Map separateExtras() {
    return {};
  }

  factory Item.createFromJson(Map json) {
    String nameOrTitle;
    if (json.containsKey('name')) {
      nameOrTitle = json['name'];
    } else {
      nameOrTitle = json['title'];
    }

    return Item(
        name: nameOrTitle,
        category: json['category'],
        imageURL: json['imageURL'],
        description: json['description'],
        user: json['user']);
  }

  factory Item.makeItem(Map json) {
    if (json['category'] == 'Clothes') {
      return Clothes.createFromJson(json);
    } else if (json['category'] == 'Printed') {
      return Prints.createFromJson(json);
    } else if (json['category'] == 'Equipment') {
      return Equipment.createFromJson(json);
    } else if (json['category'] == 'Supplies') {
      return Supplies.createFromJson(json);
    } else {
      return Item.createFromJson(json);
    }
  }
}
