import 'item.dart';

class Equipment extends Item {
  String degree;
  String type;

  Equipment(
      {required this.degree,
      required this.type,
      // Super
      required super.name,
      required super.category,
      required super.imageURL,
      required super.description,
      required super.user});

  @override
  Map separateExtras() {
    return {
      "Degree": degree,
      "Type": type,
    };
  }

  factory Equipment.createFromJson(Map json) {
    return Equipment(
        degree: json['degree'],
        type: json['type'],
        // Super
        name: json['name'],
        category: json['category'],
        imageURL: json['imageURL'],
        description: json['description'],
        user: json['user']);
  }
}
