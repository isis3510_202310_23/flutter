import 'item.dart';

class Clothes extends Item {
  String size;
  String colors;

  Clothes(
      {required this.size,
      required this.colors,
      // Super
      required super.name,
      required super.category,
      required super.imageURL,
      required super.description,
      required super.user});

  @override
  Map separateExtras() {
    return {
      "Size": size,
      "Colors": colors,
    };
  }

  factory Clothes.createFromJson(Map json) {
    return Clothes(
        size: json['size'],
        colors: json['colors'],
        // Super
        name: json['name'],
        category: json['category'],
        imageURL: json['imageURL'],
        description: json['description'],
        user: json['user']);
  }
}
