import 'item.dart';

class Prints extends Item {
  String author;
  String subject;

  Prints(
      {required this.author,
      required this.subject,
      // Super
      required super.name,
      required super.category,
      required super.imageURL,
      required super.description,
      required super.user});

  @override
  Map separateExtras() {
    return {
      "Author": author,
      "Subject": subject,
    };
  }

  factory Prints.createFromJson(Map json) {
    return Prints(
        author: json['author'],
        subject: json['subject'],
        // Super
        name: json['title'],
        category: json['category'],
        imageURL: json['imageURL'],
        description: json['description'],
        user: json['user']);
  }
}
