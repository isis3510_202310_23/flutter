import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import '../user_model/users.dart';

final UserFacade _userFacade = UserFacade();

Future<String> user = _userFacade.getUsername().then((value) => value);

// Add Clothes
Future<void> add_clothes_req(String photo, String? name, String? size,
    String? colors, String? description, String? categories) async {
  CollectionReference conection =
      FirebaseFirestore.instance.collection('Clothes_Request');
  try {
    // Add the parameters with the reference of the image in Cloud Firestore
    conection.add({
      "name": name,
      "size": size,
      "colors": colors,
      "description": description,
      "user": await user,
      "category": 'Clothes',
    }).then((value) => print(value));
  } on FirebaseException catch (e) {
    print('Failed with error code: ${e.code}');
    print(e.message);
  }
}

// Books an printed materials
Future<void> add_books_printed_req(String photo, String? title, String? author,
    String? subject, String? description, String? categories) async {
  CollectionReference conection =
      FirebaseFirestore.instance.collection('Printed_Request');
  try {
    conection.add({
      "title": title,
      "author": author,
      "subject": subject,
      "description": description,
      "user": await user,
      "category": 'Printed',
    }).then((value) {
      AlertDialog(title: Text(" Your donation has been successfully uploaded"));
    });
  } on FirebaseException catch (e) {
    print('Failed with error code: ${e.code}');
    print(e.message);
  }
}

// Protective equipment
Future<void> add_epp_req(String photo, String? name, String? degree,
    String? type, String? description, String? categories) async {
  CollectionReference conection =
      FirebaseFirestore.instance.collection('Equipment_Request');
  try {
    conection.add({
      "name": name,
      "degree": degree,
      "type": type,
      "description": description,
      "user": await user,
      "category": 'Equipment',
    }).then((value) {
      AlertDialog(title: Text(" Your donation has been successfully uploaded"));
    });
  } on FirebaseException catch (e) {
    print('Failed with error code: ${e.code}');
    print(e.message);
  }
}

// School and university supplies
Future<void> add_supplies_req(String photo, String? title, String? reference,
    String? description, String? categories) async {
  CollectionReference conection =
      FirebaseFirestore.instance.collection('Supplies_Request');
  try {
    conection.add({
      "title": title,
      "reference": reference,
      "description": description,
      "user": await user,
      "category": 'Supplies',
    }).then((value) {
      print(value);
    });
  } on FirebaseException catch (e) {
    print('Failed with error code: ${e.code}');
    print(e.message);
  }
}
