// ignore_for_file: prefer_const_constructors

import 'package:app/_Model/services/analytics_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import './connection_database_request.dart';

/**
 * Organize de data to pass to the DataBase
 */
// Save in DB
void function_DB_request(String? drop, List lista) {
  if (drop == "Clothes") {
    add_clothes_req(lista[4], lista[0], lista[1], lista[2], lista[3], drop);
  } else if (drop == "Books and Printed Materials") {
    add_books_printed_req(
        lista[4], lista[0], lista[1], lista[2], lista[3], drop);
  } else if (drop == "Protective equipment") {
    add_epp_req(lista[4], lista[0], lista[1], lista[2], lista[3], drop);
  } else if (drop == "School and university supplies") {
    add_supplies_req(lista[4], lista[0], lista[1], lista[3], drop);
  }
  AnalyticsService().logFunctionalityUse("Request");
}
