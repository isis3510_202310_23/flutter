import 'dart:ffi';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';

Future<void> add_rating(double app, double donate, double request) async {
  CollectionReference conection =
      FirebaseFirestore.instance.collection('Rating');

  try {
    await conection.add({
      "app": app,
      "donate": donate,
      "request": request,
    }).then((value) => print(value));
  } on FirebaseException catch (e) {
    print('Failed with error code: ${e.code}');
    print(e.message);
  }
}
