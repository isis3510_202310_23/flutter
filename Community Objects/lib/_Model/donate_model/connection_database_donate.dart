import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import '../user_model/users.dart';

final UserFacade _userFacade = UserFacade();

Future<String> user = _userFacade.getUsername().then((value) => value);
// Add Clothes
Future<void> add_clothes(String photo, String? name, String? size,
    String? colors, String? description, String? categories) async {
  CollectionReference conection =
      FirebaseFirestore.instance.collection('Clothes');
  try {
    var fileName = basename(photo);
    String destination = '/items/${fileName}';
    final storageRef = FirebaseStorage.instance.ref();
    var imageRef = storageRef.child(destination);
    await imageRef.putFile(File(photo));

    String URL = await imageRef.getDownloadURL();
    // Add the parameters with the reference of the image in Cloud Firestore
    conection.add({
      "imageURL": URL,
      "name": name,
      "size": size,
      "colors": colors,
      "description": description,
      "category": 'Clothes',
      "user": await user,
    }).then((value) => print(value));
  } on FirebaseException catch (e) {
    print('Failed with error code: ${e.code}');
    print(e.message);
  }
}

// Books an printed materials
Future<void> add_books_printed(String photo, String? title, String? author,
    String? subject, String? description, String? categories) async {
  CollectionReference conection =
      FirebaseFirestore.instance.collection('Printed');
  try {
    var fileName = basename(photo);
    String destination = '/items/${fileName}';
    final storageRef = FirebaseStorage.instance.ref();
    var imageRef = storageRef.child(destination);
    await imageRef.putFile(File(photo));

    String URL = await imageRef.getDownloadURL();
    conection.add({
      "imageURL": URL,
      "title": title,
      "author": author,
      "subject": subject,
      "description": description,
      "category": 'Printed',
      "user": await user,
    }).then((value) {
      AlertDialog(title: Text(" Your donation has been successfully uploaded"));
    });
  } on FirebaseException catch (e) {
    print('Failed with error code: ${e.code}');
    print(e.message);
  }
}

// Protective equipment
Future<void> add_epp(String photo, String? name, String? degree, String? type,
    String? description, String? categories) async {
  CollectionReference conection =
      FirebaseFirestore.instance.collection('Equipment');
  try {
    var fileName = basename(photo);
    String destination = '/items/${fileName}';
    final storageRef = FirebaseStorage.instance.ref();
    var imageRef = storageRef.child(destination);
    await imageRef.putFile(File(photo));

    String URL = await imageRef.getDownloadURL();
    conection.add({
      "imageURL": URL,
      "name": name,
      "degree": degree,
      "type": type,
      "description": description,
      "category": 'Equipment',
      "user": await user,
    }).then((value) {
      AlertDialog(title: Text(" Your donation has been successfully uploaded"));
    });
  } on FirebaseException catch (e) {
    print('Failed with error code: ${e.code}');
    print(e.message);
  }
}

// School and university supplies
Future<void> add_supplies(String photo, String? title, String? reference,
    String? description, String? categories) async {
  CollectionReference conection =
      FirebaseFirestore.instance.collection('Supplies');
  try {
    var fileName = basename(photo);
    String destination = '/items/${fileName}';
    final storageRef = FirebaseStorage.instance.ref();
    var imageRef = storageRef.child(destination);
    await imageRef.putFile(File(photo));

    String URL = await imageRef.getDownloadURL();
    print(_userFacade.getUsername());
    conection.add({
      "imageURL": URL,
      "title": title,
      "reference": reference,
      "description": description,
      "category": 'Supplies',
      "user": await user,
    }).then((value) {
      print(value);
    });
  } on FirebaseException catch (e) {
    print('Failed with error code: ${e.code}');
    print(e.message);
  }
}
