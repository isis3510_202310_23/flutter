import 'package:app/_View/rate%20_features/rate_features.dart';
import 'package:app/_View/request_view/Request_Objects.dart';

import 'package:app/_View/search_view/search_page.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../_Blocs/bar_bloc/bloc_bar.dart';
import '../_Model/user_model/users.dart';
import '../widgets/search_bar.dart';
import '../_View/donate_view/donate.dart';
import '../_View/request_view/Request.dart';
import '../view/AboutUs.dart';
import '../_View/contactUs _view/ContactUs.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import '../_View/home_view/home_page.dart';
import '../_View/user_view/user_page.dart';
import '../_Model/user_model/users.dart';

class bottomMenu extends StatefulWidget {
  static final bottomMenu _instance = bottomMenu._internal();

  factory bottomMenu({key}) {
    return _instance;
  }

  bottomMenu._internal() {}

  @override
  State<bottomMenu> createState() => _bottomMenuState();
}

class _bottomMenuState extends State<bottomMenu> {
  int currentTab = 0;
  TextEditingController _searchController = TextEditingController();
  final List<Widget> screens = [
    HomePage(),
    UserView(),
  ];

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = HomePage();

  @override
  Widget build(BuildContext context) {
    final UserFacade _userFacade = UserFacade();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.grey[100],
        elevation: 0.0,
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchView()));
              },
              icon: Icon(Icons.search)),
        ],
      ),
      body: screens.elementAt(currentTab),
      floatingActionButton: Visibility(
          child: SpeedDial(
        icon: Icons.add,
        backgroundColor: Color(0xff86BBD8),
        children: [
          SpeedDialChild(
            child: Icon(
              Icons.send,
            ),
            label: 'Donate',
            onTap: () {
              message(2, context);
            },
          ),
          SpeedDialChild(
            child: Icon(
              Icons.call_received,
            ),
            label: 'Request',
            onTap: () {
              message(1, context);
            },
          )
        ],
      )),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 6,
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          IconButton(
              onPressed: () {
                setState(() {
                  currentScreen = HomePage(); // Home
                  currentTab = 0;
                });
              },
              icon: Icon(
                Icons.home,
                color: currentTab == 0 ? Color(0xff86BBD8) : Colors.grey,
              )),
          IconButton(
            onPressed: () {
              setState(() {
                currentScreen = UserView(); // User
                currentTab = 1;
              });
            },
            icon: Icon(
              Icons.person,
              color: currentTab == 1 ? Color(0xff86BBD8) : Colors.grey,
            ),
          ),
        ]),
      ),
      drawer: Drawer(
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          child: ListView(
            children: [
              FutureBuilder(
                future: _userFacade.getName(),
                builder:
                    (BuildContext context, AsyncSnapshot<String> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return UserAccountsDrawerHeader(
                      accountName: Text(snapshot.data ?? ''),
                      accountEmail: FutureBuilder<String>(
                        future: _userFacade.getEmail(),
                        builder: (BuildContext context,
                            AsyncSnapshot<String> emailSnapshot) {
                          return Text(emailSnapshot.data ?? '');
                        },
                      ),
                      currentAccountPicture: FutureBuilder<String>(
                        future: _userFacade.getImagePath(),
                        builder: (BuildContext context,
                            AsyncSnapshot<String> imagePathSnapshot) {
                          return CircleAvatar(
                            backgroundColor: Color(0xff2F4858),
                            backgroundImage: AssetImage(
                                imagePathSnapshot.data ??
                                    'lib/images/avatarPPic.png'),
                            radius: 50,
                          );
                        },
                      ),
                      decoration: const BoxDecoration(
                        color: Color(0xff2F4858),
                      ),
                    );
                  } else {
                    return CircularProgressIndicator();
                  }
                },
              ),
              const Divider(
                height: 1,
                thickness: 1,
                color: Colors.white,
              ),
              const Divider(
                height: 1,
                thickness: 1,
                color: Colors.white,
              ),
              ListTile(
                title: const Text('Requests'),
                //onTap: _signUserOut,
                onTap: () {
                  message(3, context);
                },
              ),
              ListTile(
                title: const Text('About Us'),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const AboutUsPage(),
                    ),
                  );
                },
              ),
              const Divider(
                height: 1,
                thickness: 1,
                color: Colors.white,
              ),
              ListTile(
                title: const Text('Support'),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const ContactUsPage(),
                    ),
                  );
                },
              ),
              const Divider(
                height: 1,
                thickness: 1,
                color: Colors.white,
              ),
              ListTile(
                title: const Text('Sign Out'),
                //onTap: _signUserOut,
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const RatePage(),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
