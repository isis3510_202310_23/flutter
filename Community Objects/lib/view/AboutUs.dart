import 'package:app/view/bar.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutUsPage extends StatelessWidget {
  const AboutUsPage({super.key});
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[100],
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          leading: BackButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => bottomMenu(),
                ),
              );
            },
          ),
          backgroundColor: Colors.grey[100],
          elevation: 0.0,
          title: Text(
            'Return to the previous page',
            style: TextStyle(
                fontFamily: 'Roboto',
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'About Us',
                  style: TextStyle(
                      fontFamily: 'Hind_2',
                      letterSpacing: 1,
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFFF28919)),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'We are a group of passionate university students '
                  'working to make university life equally accessible '
                  'to every student, no matter their background. We aim '
                  'to follow our university’s core values primarily by'
                  ' cultivating solidarity and a sense of community for all.',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontFamily: 'Hind',
                    letterSpacing: 1,
                    fontSize: 20,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  } // Root widget[clases]
}
