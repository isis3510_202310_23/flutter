import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class Clothes_option extends StatelessWidget {
  // Controllers for Text widgets
  String? name;
  String? size;
  String? color;
  String? description;

  // Widgets
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Name', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      TextFormField(
        enableSuggestions: true,
        validator: FormBuilderValidators.minLength(3),
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]"))
        ],
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        decoration: InputDecoration(
          focusedBorder: InputBorder.none,
          labelText: 'Boots, T-shirt, pants...',
        ),
        onChanged: (value) {
          name = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Size', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      TextFormField(
        validator: FormBuilderValidators.minLength(1),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        maxLength: 7,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[0-9a-zA-Z]"))
        ],
        decoration: InputDecoration(
            focusedBorder: InputBorder.none, labelText: 'Size of the clothes'),
        onChanged: (value) {
          size = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Colors',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      TextFormField(
        autocorrect: true,
        enableSuggestions: true,
        validator: FormBuilderValidators.minLength(3),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[,a-zA-Z]"))
        ],
        obscureText: false,
        decoration: InputDecoration(
          focusedBorder: InputBorder.none,
          labelText: 'Color 1, Color 2, Color 3,...',
        ),
        onChanged: (value) {
          color = value;
        },
      ),
      SizedBox(
        height: 5,
      ),
      Text('Description',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      SizedBox(
        height: 10,
      ),
      TextFormField(
        validator: FormBuilderValidators.minLength(20),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        maxLength: 200,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        textAlignVertical: TextAlignVertical.top,
        obscureText: false,
        maxLines: 3,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: 'Give a brief description of the clothes',
          contentPadding: EdgeInsets.symmetric(vertical: 40),
        ),
        onChanged: (value) {
          description = value;
        },
      ),
    ]);
  }

  List<String?> objetos_() {
    return [name, size, color, description];
  }
}

class BandPrinted_option extends StatelessWidget {
  final titleController = TextEditingController();
  final authorController = TextEditingController();
  final subjectController = TextEditingController();
  final descriptionController = TextEditingController();

  List<String?> objetos = ['', '', '', ''];

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Title',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      TextFormField(
        controller: titleController,
        validator: FormBuilderValidators.minLength(1),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[0-9a-zA-Z]"))
        ],
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        decoration: InputDecoration(
            focusedBorder: InputBorder.none, labelText: 'Name of the book'),
        onChanged: (value) {
          objetos[0] = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Author',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      TextFormField(
        enableSuggestions: true,
        controller: authorController,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]"))
        ],
        validator: FormBuilderValidators.minLength(1),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        maxLength: 30,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        decoration: InputDecoration(
          focusedBorder: InputBorder.none,
          labelText: 'Name of the Author',
        ),
        onChanged: (value) {
          objetos[1] = value;
        },
      ),
      SizedBox(
        height: 20,
      ),
      Text('Subject',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      TextFormField(
        enableSuggestions: true,
        controller: subjectController,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]"))
        ],
        validator: FormBuilderValidators.minLength(3),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        decoration: InputDecoration(
          focusedBorder: InputBorder.none,
          labelText: 'Degree/CBU/language',
        ),
        onChanged: (value) {
          objetos[2] = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Description',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      SizedBox(
        height: 10,
      ),
      TextFormField(
        controller: descriptionController,
        validator: FormBuilderValidators.minLength(20),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        maxLength: 200,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        textAlignVertical: TextAlignVertical.top,
        obscureText: false,
        maxLines: 3,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: 'Give a brief description of the book or printed material',
          contentPadding: EdgeInsets.symmetric(vertical: 40),
        ),
        onChanged: (value) {
          objetos[3] = value;
        },
      ),
    ]);
  }

  List<String?> objetos_() {
    return objetos;
  }
}

class ProtectiveE_option extends StatelessWidget {
  final nameController = TextEditingController();
  final degreeController = TextEditingController();
  final typeController = TextEditingController();
  final descriptionController = TextEditingController();

  List<String?> objetos = ['', '', '', ''];

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Name', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      TextFormField(
        validator: FormBuilderValidators.minLength(3),
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]"))
        ],
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        decoration: InputDecoration(
          focusedBorder: InputBorder.none,
          labelText: 'Boots, T-shirt, pants...',
        ),
        onChanged: (value) {
          objetos[0] = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Degree',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      TextFormField(
        controller: degreeController,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]"))
        ],
        validator: FormBuilderValidators.minLength(3),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        decoration: InputDecoration(
          focusedBorder: InputBorder.none,
          labelText: 'In which degree is used',
        ),
        onChanged: (value) {
          objetos[1] = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Type', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      TextFormField(
        controller: typeController,
        obscureText: false,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]"))
        ],
        validator: FormBuilderValidators.minLength(3),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        decoration: InputDecoration(
          focusedBorder: InputBorder.none,
          labelText: 'Laboratory/workshop ',
        ),
        onChanged: (value) {
          objetos[2] = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Description',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      TextFormField(
        controller: descriptionController,
        validator: FormBuilderValidators.minLength(20),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        maxLength: 200,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        textAlignVertical: TextAlignVertical.top,
        obscureText: false,
        maxLines: 3,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: 'Describe your protective equipment',
          contentPadding: EdgeInsets.symmetric(vertical: 30),
        ),
        onChanged: (value) {
          objetos[3] = value;
        },
      ),
    ]);
  }

  List<String?> objetos_() {
    return objetos;
  }
}

// class SUSupplies_option extends StatelessWidget {
//   final formGlobalKey = GlobalKey<FormState>();

//   final descriptionController = TextEditingController();

//   @override
//   Widget build(BuildContext context) {
//     return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
//       Text('Title',
//           style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
//       TextFormField(
//         obscureText: false,
//         validator: FormBuilderValidators.minLength(10),
//         autovalidateMode: AutovalidateMode.onUserInteraction,
//         maxLength: 50,
//         maxLengthEnforcement: MaxLengthEnforcement.enforced,
//         inputFormatters: [
//           FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]"))
//         ],
//         key: formGlobalKey,
//         decoration: InputDecoration(
//           focusedBorder: InputBorder.none,
//           labelText: 'Calculadora/Esfero',
//         ),
//       ),
//       SizedBox(
//         height: 10,
//       ),
//       Text('Reference',
//           style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
//       TextFormField(
//         obscureText: false,
//         inputFormatters: [
//           FilteringTextInputFormatter.allow(RegExp("[0-9a-zA-Z]"))
//         ],
//         decoration: InputDecoration(
//           focusedBorder: InputBorder.none,
//           labelText: 'Texas TI-99/Bic',
//         ),
//       ),
//       SizedBox(
//         height: 10,
//       ),
//       Text('Description',
//           style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
//       TextFormField(
//         textAlignVertical: TextAlignVertical.top,
//         obscureText: false,
//         maxLines: 3,
//         decoration: InputDecoration(
//           border: OutlineInputBorder(),
//           hintText: 'Give a brief description of the object',
//           contentPadding: EdgeInsets.symmetric(vertical: 30),
//         ),
//       ),
//     ]);
//   }

//   void intento() {
//     print(descriptionController.text);
//   }
// }
