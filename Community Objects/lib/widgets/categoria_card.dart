import 'package:flutter/material.dart';
import '../_Model/models/categoria.dart';

class CategoriaCard extends StatelessWidget {
  final Categoria categoria;
  final Function(String id)? onTap;
  final bool isTopCategory;
  final bool isBottomCategory;
  final bool isMostSelected;

  const CategoriaCard({
    Key? key,
    required this.categoria,
    this.onTap,
    this.isTopCategory = false,
    this.isBottomCategory = false,
    this.isMostSelected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final titleWidget = Container(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 1),
        decoration: BoxDecoration(
          color: Color(0xff86bbd8),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Center(
          child: Text(
            categoria.nombre,
            style: TextStyle(fontFamily: 'Hind', color: Colors.white),
            textAlign: TextAlign.center,
          ),
        ));

    final mostSelectedLabel = Container(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 1),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 5, 69, 109),
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(5),
            bottomLeft: Radius.circular(5),
          ),
        ),
        child: const Center(
          child: Text(
            'Users favorite category',
            style: TextStyle(fontFamily: 'Hind', color: Colors.white),
            textAlign: TextAlign.center,
          ),
        ));

    return InkWell(
      onTap: onTap == null ? null : () => onTap!(categoria.id),
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(3),
              image: DecorationImage(
                image: AssetImage(categoria.imagen),
                fit: BoxFit.scaleDown,
              ),
            ),
          ),
          if (isMostSelected)
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: mostSelectedLabel,
            ),
          if (isTopCategory)
            Positioned(
              top: 8,
              left: 8,
              child: titleWidget,
            ),
          if (isBottomCategory)
            Positioned(
              bottom: 8,
              left: 8,
              child: titleWidget,
            ),
          if (!isTopCategory && !isBottomCategory)
            Positioned(
              top: 8,
              left: 8,
              child: titleWidget,
            ),
        ],
      ),
    );
  }
}
