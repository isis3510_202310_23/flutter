import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../_Model/models/producto.dart ';

class ProductosCarrusel extends StatelessWidget {
  final String tituloCategoria;
  final List<Producto> productos;

  const ProductosCarrusel({
    Key? key,
    required this.tituloCategoria,
    required this.productos,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int maxItems = 5;
    int itemsToShow =
        productos.length >= maxItems ? maxItems : productos.length;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tituloCategoria,
          style:
              TextStyle(fontFamily: 'Hind', fontSize: 24, color: Colors.black),
          textAlign: TextAlign.start,
        ),
        SizedBox(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(itemsToShow + 1, (index) {
                if (index == itemsToShow) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                      onTap: () {
                        // Implementar la lógica para abrir la página de la categoría aquí
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.3,
                          height: MediaQuery.of(context).size.width * 0.3,
                          color: Colors.grey[200],
                          child: Icon(
                            Icons.add,
                            size: MediaQuery.of(context).size.width * 0.2,
                            color: Colors.grey[800],
                          ),
                        ),
                      ),
                    ),
                  );
                } else {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            decoration: BoxDecoration(
                              color: Color(0xff86bbd8),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        topRight: Radius.circular(10)),
                                    child: CachedNetworkImage(
                                      imageUrl: productos[index].imagenUrl,
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                      width: MediaQuery.of(context).size.width *
                                          0.3,
                                      height: MediaQuery.of(context).size.width *
                                          0.3,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  Text(
                                    productos[index].nombre,
                                    style: TextStyle(
                                        fontFamily: 'Hind_2',
                                        fontSize: 14,
                                        color: Colors.white),
                                    textAlign: TextAlign.right,
                                  ),
                                ])),
                        SizedBox(height: 5),
                      ],
                    ),
                  );
                }
              }),
            ),
          ),
        ),
      ],
    );
  }
}
