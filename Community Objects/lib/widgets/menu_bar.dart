import 'package:flutter/material.dart';
import '../_Model/user_model/users.dart';
import '../_View/contactUs _view/ContactUs.dart';
import '../_View/user_view/user_donations.dart';
import '../_View/user_view/user_page.dart';
import '../view/AboutUs.dart';

class COMenu extends StatelessWidget {
  final UserFacade _userFacade = UserFacade();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        child: ListView(
          children: [
            FutureBuilder(
              future: _userFacade.getName(),
              builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return UserAccountsDrawerHeader(
                    accountName: Text(snapshot.data ?? ''),
                    accountEmail: FutureBuilder<String>(
                      future: _userFacade.getEmail(),
                      builder: (BuildContext context,
                          AsyncSnapshot<String> emailSnapshot) {
                        return Text(emailSnapshot.data ?? '');
                      },
                    ),
                    currentAccountPicture: FutureBuilder<String>(
                      future: _userFacade.getImagePath(),
                      builder: (BuildContext context,
                          AsyncSnapshot<String> imagePathSnapshot) {
                        return CircleAvatar(
                          backgroundColor: Color(0xff2F4858),
                          backgroundImage: AssetImage(imagePathSnapshot.data ??
                              'lib/images/avatarPPic.png'),
                          radius: 50,
                        );
                      },
                    ),
                    decoration: const BoxDecoration(
                      color: Color(0xff2F4858),
                    ),
                  );
                } else {
                  return CircularProgressIndicator();
                }
              },
            ),
            ListTile(
              title: const Text('Profile'),
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => UserView(),
                  ),
                );
              },
            ),
            const Divider(
              height: 1,
              thickness: 1,
              color: Colors.white,
            ),
            ListTile(
              title: const Text('Settings'),
              onTap: () {
                // add functionality here
              },
            ),
            const Divider(
              height: 1,
              thickness: 1,
              color: Colors.white,
            ),
            ListTile(
              title: const Text('About Us'),
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const AboutUsPage(),
                  ),
                );
              },
            ),
            const Divider(
              height: 1,
              thickness: 1,
              color: Colors.white,
            ),
            ListTile(
              title: const Text('Support'),
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ContactUsPage(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
