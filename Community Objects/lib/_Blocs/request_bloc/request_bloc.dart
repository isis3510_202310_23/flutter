import 'package:flutter/material.dart';

List<Widget> clothes_option(_alldonations_clothes) {
  return [
    ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        itemCount: _alldonations_clothes.length,
        itemBuilder: (context, i) {
          return ListTile(
            contentPadding: EdgeInsets.fromLTRB(25, 5, 25, 0),
            title: Text(
              "${_alldonations_clothes[i]['name']}",
              style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("${_alldonations_clothes[i]['description']}"),
              ],
            ),
          );
        })
  ];
}

List<Widget> printed_option(_alldonations_Printed) {
  return [
    ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        itemCount: _alldonations_Printed.length,
        itemBuilder: (context, i) {
          return ListTile(
            contentPadding: EdgeInsets.fromLTRB(25, 5, 25, 0),
            title: Text(
              "${_alldonations_Printed[i]['title']}",
              style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("${_alldonations_Printed[i]['description']}"),
              ],
            ),
          );
        })
  ];
}
