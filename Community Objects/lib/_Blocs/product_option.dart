// ignore_for_file: prefer_const_constructors

import '../_View/optionClases.dart';

/**
 * File that has the logic for the dropdown and the dialogs depending on the cateogry
 * 1. Option categories: Changes the view depending on which is seleted
 * 2. Which view the dialog erase the content
 * 3. 
 */

// Select the menu displayed depending on the dropdown
optionClases options_categories(String? option) {
  var selection;
  if (option == "Clothes") {
    selection = optionClases(1);
  } else if (option == "Books and Printed Materials") {
    selection = optionClases(2);
  } else if (option == "Protective equipment") {
    selection = optionClases(3);
  } else if (option == 'School and university supplies') {
    selection = optionClases(4);
  }
  ;
  return selection;
}

// Erase the content
void delete_content_categories(String? option) {
  var selection;
  if (option == "Clothes") {
    clean_clothes();
  } else if (option == "Books and Printed Materials") {
    clean_printed();
  } else if (option == "Protective equipment") {
    clean_protective();
  } else if (option == 'School and university supplies') {
    clean_supplies();
  }
  ;
}
