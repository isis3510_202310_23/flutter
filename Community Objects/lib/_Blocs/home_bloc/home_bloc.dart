import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import '../../_Model/models/item.dart';
import '../../_Model/services/item_service.dart';
import '../../_Model/models/categoria.dart';
import '../category_bloc/categorias.dart';

class HomeBloc {
  List<Categoria> categoriasOrdenadas = ordenarCategorias(categorias);

  late int clickedCategoriaIndex;

  late StreamSubscription<String> tokenSubscriptionStream;
  String token = "NO TOKEN";

  Future getPopularities() async {
    final snapshot = await ItemServices().getCategoryPopularities();
    Map json = snapshot.data() as Map<dynamic, dynamic>;
    for (var element in categoriasOrdenadas) {
      element.totalSeleccionado = json[element.dbName];
    }
  }

  Future updatePopularity(String categoryName) async {
    ItemServices().updateCategoryPopularity(categoryName);
  }

  Future loadCategory(String categoryName) async {
    final snapshot = await ItemServices().getItemsFromCategory(categoryName);
    if (snapshot.size > 0) {
      List<Item> result = [];
      for (var element in snapshot.docs) {
        Item i = Item.createFromJson(element.data() as Map);
        i.setId(element.id);
        result.add(i);
      }
      return result;
    } else {
      return null;
    }
  }

  String actualizarCategoria(String id) {
    final clickedCategoriaIndex =
        categoriasOrdenadas.indexWhere((cat) => cat.id == id);
    final clickedCategoria = categoriasOrdenadas[clickedCategoriaIndex];
    clickedCategoria.incrementarTotalSeleccionado();

    updatePopularity(clickedCategoria.dbName);

    categoriasOrdenadas
        .sort((a, b) => b.totalSeleccionado.compareTo(a.totalSeleccionado));

    if (kDebugMode) {
      print(categoriasOrdenadas.first.totalSeleccionado);
    }

    return clickedCategoria.dbName;
  }

  // Token for Notifications
  void setTokenSubscriptionStream() {
    tokenSubscriptionStream = FirebaseMessaging.instance.onTokenRefresh.listen(
      (String token) {
        if (kDebugMode) {
          print("NEW TOKEN: $token");
        }
        token = token;

        FirebaseFirestore.instance.collection('users').add({
          'email': FirebaseAuth.instance.currentUser?.email,
          'token': token,
        });
      },
    );
  }
}
