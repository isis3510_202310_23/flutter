import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import '../../_View/donate_view/donate.dart';
import '../../_View/request_view/Request.dart';
import '../../_View/request_view/Request_Objects.dart';

// Show dialogs
Future<dynamic> dialog(String message, BuildContext context) {
  return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          title: Text(message),
          titleTextStyle:
              TextStyle(color: Colors.black, fontSize: 16, fontFamily: 'Hind'),
          actions: <Widget>[
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Color(0xFF86BBD8)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text("Done")),
          ],
        );
      });
}

//'Please Check your internet connection, we can\'t show you the email apps'
void message(int issue, BuildContext context) async {
  bool result = await InternetConnectionChecker().hasConnection;
  if (issue == 1) {
    if (result == true)
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => RequestPage(),
        ),
      );
    else {
      dialog('Please Check your internet connection, this feature need it',
          context);
    }
  } else if (issue == 2) {
    if (result == true) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DonatePage(),
        ),
      );
    } else {
      dialog('Please Check your internet connection, this feature need it',
          context);
    }
  } else if (issue == 3) {
    if (result == true) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => RequestObjects_(),
        ),
      );
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => RequestObjects_(),
        ),
      );
      dialog(
          'Please check your connection, the information displayed is not up to date.',
          context);
    }
  }
}
