import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:rxdart/rxdart.dart';

class ConnectionBloc {
  final _connectionStatusSubject = BehaviorSubject<ConnectivityResult>();

  ConnectionBloc() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatusSubject.sink.add(result);
    });
  }

  Stream<ConnectivityResult> get connectionStatus => _connectionStatusSubject.stream;

  void dispose() {
    _connectionStatusSubject.close();
  }
}
