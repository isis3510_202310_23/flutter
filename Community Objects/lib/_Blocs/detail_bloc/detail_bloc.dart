import 'package:app/_Model/services/analytics_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../_Model/models/item.dart';
import '../../_Model/services/item_service.dart';

class DetailBloc {
  static late Item item;
  static late List<MapEntry> extras;

  Future logUse() async {
    AnalyticsService().logFunctionalityUse("View_Product");
  }

  Future<DocumentSnapshot> loadItem(String itemId, String categoryName) async {
    final snapshot = await ItemServices().getItem(itemId, categoryName);
    if (snapshot.exists) {
      Map json = snapshot.data() as Map;

      // Extract item defaults
      item = Item.makeItem(json);
      extras = item.separateExtras().entries.toList();

      return snapshot;
    } else {
      return snapshot;
    }
  }

  Item getItem() {
    return item;
  }

  List<MapEntry> getExtras() {
    return extras;
  }
}
