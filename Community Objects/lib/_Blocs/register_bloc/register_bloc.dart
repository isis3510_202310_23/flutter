class RegisterBloc {
  String? validateDefault(String? dato) {
    if (dato!.isEmpty) {
      return "This field is required.";
    }
    if (dato.length > 30) {
      return "This field can't be longer than 30 characters.";
    }
    return null;
  }

  String? validateAge(String? dato) {
    if (dato!.isEmpty) {
      return "This field is required.";
    }
    if (dato.length > 2) {
      return "The age can't be longer than 2 characters.";
    }
    if (num.tryParse(dato) == null) {
      return "The age must be a number.";
    }
    if (num.parse(dato) <= 16) {
      return "You must be older than 16 to use this app.";
    }
    return null;
  }

  String? validateEmail(String? dato) {
    if (dato!.isEmpty) {
      return "This field is required.";
    }
    if (!dato.endsWith("@uniandes.edu.co")) {
      return "You must register using an Uniandes email.";
    }
    return null;
  }

  String? validatePassword(String? dato) {
    if (dato!.isEmpty) {
      return "This field is required.";
    }
    if (dato.length < 6) {
      return "The password must be longer than 6 characters.";
    }
    return null;
  }
}
