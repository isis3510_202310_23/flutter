import '../../_Model/models/categoria.dart';

List<Categoria> categorias = [
  Categoria(
      id: '1',
      nombre: 'Clothes',
      imagen: 'lib/images/electronics/clothes.png',
      dbName: "Clothes",
      totalSeleccionado: 0),
  Categoria(
      id: '2',
      nombre: 'EPP',
      imagen: 'lib/images/labs/EPP.png',
      dbName: "Equipment",
      totalSeleccionado: 0),
  Categoria(
      id: '3',
      nombre: 'B&PM',
      imagen: 'lib/images/textbooks/book.png',
      dbName: "Printed",
      totalSeleccionado: 0),
  Categoria(
      id: '4',
      nombre: 'S&U Supplies',
      imagen: 'lib/images/utiles/supplies.png',
      dbName: "Supplies",
      totalSeleccionado: 0),
];

List<Categoria> ordenarCategorias(List<Categoria> categorias) {
  categorias.sort((a, b) => b.totalSeleccionado.compareTo(b.totalSeleccionado));
  return categorias;
}
