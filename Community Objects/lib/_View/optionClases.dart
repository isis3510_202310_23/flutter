import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:path/path.dart';

// Controllers Clothes supplies option
final controller_c1 = TextEditingController();
final controller_c2 = TextEditingController();
final controller_c3 = TextEditingController();
final controller_c4 = TextEditingController();

// Controllers Printed supplies option
final controller_p1 = TextEditingController();
final controller_p2 = TextEditingController();
final controller_p3 = TextEditingController();
final controller_p4 = TextEditingController();

// Controllers Protective equipment option
final controller_b1 = TextEditingController();
final controller_b2 = TextEditingController();
final controller_b3 = TextEditingController();
final controller_b4 = TextEditingController();

// Controllers Supplies supplies option
final controller_s1 = TextEditingController();
final controller_s2 = TextEditingController();
final controller_s3 = TextEditingController();

class optionClases extends StatelessWidget {
  optionClases(int this.option);

  List<String?> objetos = ['', '', '', '', ''];
  int option;

  List<String?> objects() {
    return objetos;
  }

  // Widgets
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: (option == 1)
            ? clothes_option(
                controller_c1, controller_c2, controller_c3, controller_c4)
            : (option == 2)
                ? bAuthor_option(
                    controller_b1, controller_b2, controller_b3, controller_b4)
                : (option == 3)
                    ? protective_option(controller_p1, controller_p2,
                        controller_p3, controller_p4)
                    : (option == 4)
                        ? SUSupplies_option(
                            controller_s1, controller_s2, controller_s3)
                        : clothes_option(controller_c1, controller_c2,
                            controller_c3, controller_c4));
  }

  List<Widget> clothes_option(final controller_c1, final controller_c2,
      final controller_c3, final controller_c4) {
    return <Widget>[
      Text('Name',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      TextFormField(
        controller: controller_c1,
        autovalidateMode: AutovalidateMode.disabled,
        enableSuggestions: true,
        style: TextStyle(color: Colors.white),
        validator: FormBuilderValidators.minLength(3),
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]"))
        ],
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'Boots, T-shirt, pants...',
        ),
        onChanged: (value) {
          objetos[0] = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Size',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      TextFormField(
        controller: controller_c2,
        validator: FormBuilderValidators.minLength(1),
        style: TextStyle(color: Colors.white),
        autovalidateMode: AutovalidateMode.disabled,
        maxLength: 7,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[0-9 /a-zA-Z]"))
        ],
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'Size of the clothes',
        ),
        onChanged: (value) {
          objetos[1] = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Colors',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      TextFormField(
        controller: controller_c3,
        style: TextStyle(color: Colors.white),
        autocorrect: true,
        enableSuggestions: true,
        validator: FormBuilderValidators.minLength(3),
        autovalidateMode: AutovalidateMode.disabled,
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[ ,a-zA-Z]"))
        ],
        obscureText: false,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'Color 1, Color 2, Color 3,...',
        ),
        onChanged: (value) {
          objetos[2] = value;
        },
      ),
      SizedBox(
        height: 5,
      ),
      Text('Description',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      SizedBox(
        height: 10,
      ),
      TextFormField(
        controller: controller_c4,
        style: TextStyle(color: Colors.white),
        validator: FormBuilderValidators.compose([
          FormBuilderValidators.required(),
          FormBuilderValidators.minLength(20),
        ]),
        autovalidateMode: AutovalidateMode.disabled,
        maxLength: 200,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        textAlignVertical: TextAlignVertical.top,
        obscureText: false,
        maxLines: 3,
        decoration: InputDecoration(
          enabledBorder:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'Give a brief description of the clothes',
        ),
        onChanged: (value) {
          objetos[3] = value;
        },
      )
    ];
  }

  List<Widget> protective_option(
      controller_p1, controller_p2, controller_p3, controller_p4) {
    // Controllers Books and Printed Materials option

    return <Widget>[
      Text('Name',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      TextFormField(
        controller: controller_p1,
        autovalidateMode: AutovalidateMode.disabled,
        validator: FormBuilderValidators.minLength(3),
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-z A-Z]"))
        ],
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        enableSuggestions: true,
        style: TextStyle(color: Colors.white),
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'Boots, T-shirt, pants...',
        ),
        onChanged: (value) {
          objetos[0] = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Degree',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      TextFormField(
        controller: controller_p2,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-z A-Z]"))
        ],
        validator: FormBuilderValidators.minLength(3),
        autovalidateMode: AutovalidateMode.disabled,
        maxLength: 50,
        style: TextStyle(color: Colors.white),
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'In which degree is used',
        ),
        onChanged: (value) {
          objetos[1] = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Type',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      TextFormField(
        controller: controller_p3,
        obscureText: false,
        style: TextStyle(color: Colors.white),
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-z A-Z]"))
        ],
        validator: FormBuilderValidators.minLength(3),
        autovalidateMode: AutovalidateMode.disabled,
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'Laboratory/workshop ',
        ),
        onChanged: (value) {
          objetos[2] = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Description',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      TextFormField(
        controller: controller_p4,
        style: TextStyle(color: Colors.white),
        validator: FormBuilderValidators.minLength(20),
        autovalidateMode: AutovalidateMode.disabled,
        maxLength: 200,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        textAlignVertical: TextAlignVertical.top,
        obscureText: false,
        maxLines: 3,
        decoration: InputDecoration(
          enabledBorder:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'Describe your protective equipment',
        ),
        onChanged: (value) {
          objetos[3] = value;
        },
      ),
    ];
  }

  List<Widget> bAuthor_option(
      controller_b1, controller_b2, controller_b3, controller_b4) {
    return <Widget>[
      Text('Title',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      TextFormField(
        controller: controller_b1,
        style: TextStyle(color: Colors.white),
        validator: FormBuilderValidators.minLength(1),
        autovalidateMode: AutovalidateMode.disabled,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[0-9, a-zA-Z]"))
        ],
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            labelStyle: TextStyle(color: Color(0xFF86BBD8)),
            labelText: 'Name of the book'),
        onChanged: (value) {
          objetos[0] = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Author',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      TextFormField(
        controller: controller_b2,
        style: TextStyle(color: Colors.white),
        enableSuggestions: true,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-z A-Z]"))
        ],
        validator: (value) {
          FormBuilderValidators.minLength(1);
        },
        autovalidateMode: AutovalidateMode.disabled,
        maxLength: 30,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'Name of the Author',
        ),
        onChanged: (value) {
          objetos[1] = value;
        },
      ),
      SizedBox(
        height: 20,
      ),
      Text('Subject',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      TextFormField(
        controller: controller_b3,
        style: TextStyle(color: Colors.white),
        enableSuggestions: true,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-z A-Z]"))
        ],
        validator: (value) {
          FormBuilderValidators.minLength(3);
        },
        autovalidateMode: AutovalidateMode.disabled,
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        obscureText: false,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'Degree/CBU/language',
        ),
        onChanged: (value) {
          objetos[2] = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Description',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      SizedBox(
        height: 10,
      ),
      TextFormField(
        controller: controller_b4,
        style: TextStyle(color: Colors.white),
        validator: FormBuilderValidators.minLength(20),
        autovalidateMode: AutovalidateMode.disabled,
        maxLength: 200,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        textAlignVertical: TextAlignVertical.top,
        obscureText: false,
        maxLines: 3,
        decoration: InputDecoration(
          enabledBorder:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'Give a brief description of the book or printed material',
        ),
        onChanged: (value) {
          objetos[3] = value;
        },
      )
    ];
  }

  List<Widget> SUSupplies_option(controller_s1, controller_s2, controller_s3) {
    return <Widget>[
      Text('Title',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      TextFormField(
        style: TextStyle(color: Colors.white),
        controller: controller_s1,
        obscureText: false,
        validator: (value) {
          FormBuilderValidators.minLength(10);
        },
        autovalidateMode: AutovalidateMode.disabled,
        maxLength: 50,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-z A-Z]"))
        ],
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'Calculator/pencil',
        ),
        onChanged: (value) {
          objetos[0] = value;
        },
      ),
      SizedBox(
        height: 10,
      ),
      Text('Reference',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      TextFormField(
        style: TextStyle(color: Colors.white),
        controller: controller_s2,
        validator: FormBuilderValidators.minLength(3),
        obscureText: false,
        maxLength: 20,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[0-9 a-zA-Z]"))
        ],
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'Texas TI-99/Bic',
        ),
        onChanged: (value) {
          objetos[1] = value;
        },
      ),
      SizedBox(
        height: 15,
      ),
      Text('Description',
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
      SizedBox(
        height: 10,
      ),
      TextFormField(
        controller: controller_s3,
        style: TextStyle(color: Colors.white),
        validator: FormBuilderValidators.compose([
          FormBuilderValidators.required(),
          FormBuilderValidators.minLength(20),
        ]),
        autovalidateMode: AutovalidateMode.disabled,
        maxLength: 200,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        textAlignVertical: TextAlignVertical.top,
        obscureText: false,
        maxLines: 3,
        decoration: InputDecoration(
          enabledBorder:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
          labelStyle: TextStyle(color: Color(0xFF86BBD8)),
          labelText: 'Give a brief description of the object',
        ),
        onChanged: (value) {
          objetos[3] = value;
        },
      )
    ];
  }
}

// Functions to clear the cells after the submition of the products
Future<void> clean_clothes() async {
  controller_c1.clear();
  controller_c2.clear();
  controller_c3.clear();
  controller_c4.clear();
}

Future<void> clean_protective() async {
  controller_p1.clear();
  controller_p2.clear();
  controller_p3.clear();
  controller_p4.clear();
}

Future<void> clean_printed() async {
  controller_b1.clear();
  controller_b2.clear();
  controller_b3.clear();
  controller_b4.clear();
}

Future<void> clean_supplies() async {
  controller_s1.clear();
  controller_s2.clear();
  controller_s3.clear();
}
