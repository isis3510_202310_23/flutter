import 'package:app/_Model/services/analytics_service.dart';
import 'package:app/_View/custom_widgets/my_formfield.dart';
import '../../_Blocs/register_bloc/register_bloc.dart';
import '../../_Model/user_model/user_repositrory.dart';
import '../custom_widgets/my_arc.dart';
import '../custom_widgets/my_button.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  final Function()? onTap;

  const RegisterPage({super.key, required this.onTap});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  // controllers
  final nameController = TextEditingController();
  final genderController = TextEditingController();
  final ageController = TextEditingController();
  final usernameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  //Repository
  final UserRepository userRepository = UserRepositoryImpl();

  // methods
  void signUserUp() async {
    if (formKey.currentState!.validate()) {
      String message = await userRepository.registerUser(
          nameController.text,
          genderController.text,
          ageController.text,
          usernameController.text,
          emailController.text,
          passwordController.text);

      AnalyticsService().logSignUp();

      if (message != "Success") {
        showErrorMessage(message);
      }
    }
  }

  // Wrong User Method
  void showErrorMessage(String message) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(message),
        );
      },
    );
  }

  // Validators

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromARGB(255, 7, 57, 60),
        body: SafeArea(
          child: Center(
            child: Form(
              key: formKey,
              child: SingleChildScrollView(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      MyArc(
                        diameter: MediaQuery.of(context).size.width * .50,
                        inside: Image.asset(
                          'lib/images/Logo.jpg',
                        ),
                      ),

                      const SizedBox(height: 25),

                      // Name textfield
                      MyFormField(
                        controller: nameController,
                        hintText: "Name",
                        obscureText: false,
                        preIcon: const Icon(Icons.person),
                        validatorMethod: RegisterBloc().validateDefault,
                      ),

                      const SizedBox(height: 10),

                      // Gender textfield
                      MyFormField(
                        controller: genderController,
                        hintText: "Gender (NA if you prefer)",
                        obscureText: false,
                        preIcon: const Icon(Icons.settings),
                        validatorMethod: RegisterBloc().validateDefault,
                      ),

                      const SizedBox(height: 10),

                      // Age textfield
                      MyFormField(
                        controller: ageController,
                        hintText: "Age (Enter the number)",
                        obscureText: false,
                        preIcon: const Icon(Icons.numbers),
                        validatorMethod: RegisterBloc().validateAge,
                      ),

                      const SizedBox(height: 10),

                      // username textfield
                      MyFormField(
                        controller: usernameController,
                        hintText: "Username",
                        obscureText: false,
                        preIcon: const Icon(Icons.account_circle),
                        validatorMethod: RegisterBloc().validateDefault,
                      ),

                      const SizedBox(height: 10),

                      // email textfield
                      MyFormField(
                        controller: emailController,
                        hintText: "Email",
                        obscureText: false,
                        preIcon: const Icon(Icons.email),
                        validatorMethod: RegisterBloc().validateEmail,
                      ),

                      const SizedBox(height: 10),

                      // password textfield
                      MyFormField(
                        controller: passwordController,
                        hintText: "Password",
                        obscureText: false,
                        preIcon: const Icon(Icons.lock),
                        validatorMethod: RegisterBloc().validatePassword,
                      ),

                      const SizedBox(height: 20),

                      // signin button
                      MyButton(
                        onTap: signUserUp,
                        text: "Sign Up",
                      ),

                      const SizedBox(height: 25),

                      // Change back to login
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            'Already a user?',
                            style: TextStyle(
                                fontFamily: 'Microsoft-JengHei',
                                fontSize: 20,
                                color: Color(0xff86BBD8)),
                          ),
                          const SizedBox(width: 4),
                          GestureDetector(
                            onTap: widget.onTap,
                            child: const Text('Login',
                                style: TextStyle(
                                    fontFamily: 'Microsoft-JengHei',
                                    fontSize: 20,
                                    color: Color(0xff86BBD8))),
                          ),
                        ],
                      )
                    ]),
              ),
            ),
          ),
        ));
  }
}
