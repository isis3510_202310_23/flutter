import 'dart:async';
import 'package:flutter/material.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

import '../../_Blocs/connection_bloc/connection_bloc.dart';
import '../../_Model/user_model/users.dart';
import '../../widgets/menu_bar.dart';

class UserDonationsView extends StatefulWidget {
  @override
  _UserDonationsViewState createState() => _UserDonationsViewState();
}

class _UserDonationsViewState extends State<UserDonationsView> {
  final UserFacade _userFacade = UserFacade();
  final ConnectionBloc _bloc = ConnectionBloc();
  late StreamSubscription<ConnectivityResult> _subscription;

  @override
  void initState() {
    super.initState();
    _subscription = _bloc.connectionStatus.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        _showConnectionLostDialog(context);
      }
    });
  }

  void _showConnectionLostDialog(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Connection Lost'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Your internet connection has been lost. This app requires a stable internet connection to function properly.',
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Community Objects',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Color(0xff2F4858),
      ),
      drawer: COMenu(),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.all(16),
              child: FutureBuilder<String>(
                future: _userFacade.getName(),
                builder: (BuildContext context, AsyncSnapshot<String> nameSnapshot) {
                  if (nameSnapshot.connectionState == ConnectionState.done) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          nameSnapshot.data ?? '',
                          style: TextStyle(
                            fontSize: 40,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff2F4858),
                          ),
                        ),
                        SizedBox(height: 8),
                        FutureBuilder<String>(
                          future: _userFacade.getUsername(),
                          builder: (BuildContext context, AsyncSnapshot<String> usernameSnapshot) {
                            if (usernameSnapshot.connectionState == ConnectionState.done) {
                              return Text(
                                usernameSnapshot.data ?? '',
                                style: TextStyle(
                                  fontSize: 22,
                                ),
                              );
                            } else {
                              return CircularProgressIndicator();
                            }
                          },
                        ),
                      ],
                    );
                  } else {
                    return CircularProgressIndicator();
                  }
                },
              ),
            ),
            SizedBox(height: 16),
            Center(
              child: Text(
                'Your Donations',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
           // FutureBuilder<List<dynamic>>(
             // future: _getUserDonations(),
              //builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
                //if (snapshot.connectionState == ConnectionState.done) {
                  //if (snapshot.hasData) {
                    //final donations = snapshot.data!;
                    // Column(
                      //children: donations.map((donation) {
                        // Customize the UI for each donation item
                        //return ListTile(
                          //title: Text(donation['name']),
                          //subtitle: Text(donation['description']),
                          //leading: Image.network(donation['imageURL']),
                          // Add more details or customize as needed
                        //);
                      //}).toList(),
                   // );
                 // } else {
                  //  return Text('No donations found.');
                 // }
              //  } else {
                 // return CircularProgressIndicator();
               // }
             // },
           // ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }
}
