import 'dart:async';
import 'package:app/_View/user_view/user_donations.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

import '../../_Blocs/connection_bloc/connection_bloc.dart';
import '../../_View/home_view/home_page.dart';
import '../../_Model/user_model/users.dart';
import 'package:flutter/material.dart';

class UserView extends StatelessWidget {
  UserView({Key? key}) : super(key: key);

  final UserFacade _userFacade = UserFacade();

  final ConnectionBloc bloc = ConnectionBloc();

  void _showConnectionLostDialog(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Connection Lost'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Your internet connection has been lost. This app requires a stable internet connection to function properly.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<ConnectivityResult>(
      stream: bloc.connectionStatus,
      builder: (BuildContext context,
          AsyncSnapshot<ConnectivityResult> snapshot) {
        if (snapshot.hasData && snapshot.data == ConnectivityResult.none) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            _showConnectionLostDialog(context);
          });
        }
        return Scaffold(
          body: SafeArea(
            child: Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 3 / 7,
                  decoration: BoxDecoration(
                    color: Color(0xff2F4858),
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(70),
                      bottomLeft: Radius.circular(70),
                    ),
                  ),
                ),
                SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05),
                      FutureBuilder<String>(
                        future: _userFacade.getImagePath(),
                        builder: (BuildContext context,
                            AsyncSnapshot<String> imagePathSnapshot) {
                          if (imagePathSnapshot.connectionState ==
                              ConnectionState.done) {
                            return CircleAvatar(
                              radius: 50,
                              backgroundImage: AssetImage(
                                  imagePathSnapshot.data ??
                                      'lib/images/avatarPPic.png'),
                            );
                          } else {
                            return CircularProgressIndicator();
                          }
                        },
                      ),
                      const SizedBox(height: 10),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.white,
                          border: Border.all(
                            color: Color(0xff86BBD8),
                            width: 1.5,
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '0',
                                      style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xff86BBD8),
                                      ),
                                    ),
                                    const SizedBox(height: 5),
                                    const Text(
                                      'Acquisitions',
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Color(0xff86BBD8),
                                      ),
                                    ),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => UserDonationsView(),
                                        ),
                                      );
                                    },
                                    child: FutureBuilder<String>(
                                      future: _userFacade.getDonations(),
                                      builder: (BuildContext context, AsyncSnapshot<String> donationsSnapshot) {
                                        if (donationsSnapshot.connectionState == ConnectionState.done) {
                                          return Column(
                                            children: [
                                              Text(
                                                donationsSnapshot.data ?? '0',
                                                style: TextStyle(
                                                  fontSize: 24,
                                                  fontWeight: FontWeight.bold,
                                                  color: Color(0xff86BBD8),
                                                ),
                                              ),
                                              const SizedBox(height: 5),
                                              const Text(
                                                'Donations',
                                                style: TextStyle(
                                                  fontSize: 15,
                                                  color: Color(0xff86BBD8),
                                                ),
                                              ),
                                            ],
                                          );
                                        } else {
                                          return CircularProgressIndicator();
                                        }
                                      },
                                    ),
                                  ),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '0',
                                      style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xff86BBD8),
                                      ),
                                    ),
                                    const SizedBox(height: 5),
                                    const Text(
                                      'Requests',
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Color(0xff86BBD8),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            const SizedBox(height: 20),
                            const Text(
                              'Name:',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff86BBD8)),
                            ),
                            const SizedBox(height: 5),
                            FutureBuilder<String>(
                              future: _userFacade.getName(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<String> nameSnapshot) {
                                if (nameSnapshot.connectionState ==
                                    ConnectionState.done) {
                                  return Text(
                                    nameSnapshot.data ?? '',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Color(0xff86BBD8)),
                                  );
                                } else {
                                  return CircularProgressIndicator();
                                }
                              },
                            ),
                            const SizedBox(height: 10),
                            // Add Gender                            
                            const Text('Email:',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff86BBD8)),
                            ),
                            const SizedBox(height: 5),
                            FutureBuilder<String>(
                              future: _userFacade.getEmail(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<String> nameSnapshot) {
                                if (nameSnapshot.connectionState ==
                                    ConnectionState.done) {
                                  return Text(
                                    nameSnapshot.data ?? '',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Color(0xff86BBD8)),
                                  );
                                } else {
                                  return CircularProgressIndicator();
                                }
                              },
                            ),
                            const SizedBox(height: 10),
                            // Add Gender
                            const Text(
                              'Gender:',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff86BBD8)),
                            ),
                            const SizedBox(height: 5),
                            FutureBuilder<String>(
                              future: _userFacade.getGender(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<String> genderSnapshot) {
                                if (genderSnapshot.connectionState ==
                                    ConnectionState.done) {
                                  return Text(
                                    genderSnapshot.data ?? '',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Color(0xff86BBD8)),
                                  );
                                } else {
                                  return CircularProgressIndicator();
                                }
                              },
                            ),
                            const SizedBox(height: 10),
                            // Add Career
                            const Text(
                              'Career:',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff86BBD8)),
                            ),
                            const SizedBox(height: 5),
                            FutureBuilder<String>(
                              future: _userFacade.getCareer(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<String> careerSnapshot) {
                                if (careerSnapshot.connectionState ==
                                    ConnectionState.done) {
                                  return Text(
                                    careerSnapshot.data ?? '',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Color(0xff86BBD8)),
                                  );
                                } else {
                                  return CircularProgressIndicator();
                                }
                              },
                            ),
                            const SizedBox(height: 10),
                            // Add Age
                            const Text(
                              'Age:',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff86BBD8)),
                            ),
                            const SizedBox(height: 5),
                            FutureBuilder<String>(
                              future: _userFacade.getAge(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<String> ageSnapshot) {
                                if (ageSnapshot.connectionState ==
                                    ConnectionState.done) {
                                  return Text(
                                    ageSnapshot.data ?? '',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Color(0xff86BBD8)),
                                  );
                                } else {
                                  return CircularProgressIndicator();
                                }
                              },
                            ),
                            const SizedBox(height: 20),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                ElevatedButton(
                                  onPressed: () {
                                    UserFacade.editProfile();
                                  },
                                  child: const Text(
                                    'Edit Profile',
                                    style: TextStyle(color: Color(0xff2F4858)),
                                  ),
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: Colors.white,
                                    side: BorderSide(
                                      color: Color(0xff86BBD8),
                                      width: 1.5,
                                    ),
                                  ).copyWith(
                                    elevation: MaterialStateProperty.resolveWith<double>(
                                        (states) => 4.0),
                                  ),
                                ),
                                ElevatedButton(
                                  onPressed: _userFacade.logout,
                                  child: const Text(
                                    'Logout',
                                    style: TextStyle(color: Color(0xff2F4858)),
                                  ),
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: Colors.white,
                                    side: BorderSide(
                                        color: Color(0xff86BBD8), width: 1.5),
                                  ).copyWith(
                                    elevation: MaterialStateProperty.resolveWith<double>(
                                        (states) => 4.0),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
