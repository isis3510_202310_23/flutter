import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import '../../view/bar.dart';
import '../../_Blocs/request_bloc/request_bloc.dart';

class RequestObjects extends StatelessWidget {
  const RequestObjects({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: const RequestObjects_(),
    );
  }
}

class RequestObjects_ extends StatefulWidget {
  const RequestObjects_({Key? key}) : super(key: key);

  @override
  State<RequestObjects_> createState() => _RequestObjects();
}

class _RequestObjects extends State<RequestObjects_> {
  // Clothes
  List _alldonations_clothes = [];
  CollectionReference user_clothes =
      FirebaseFirestore.instance.collection('Clothes_Request');
  getData_clothes() async {
    var responsebody = await user_clothes.get();
    responsebody.docs.forEach((element) {
      setState(() {
        _alldonations_clothes.add(element.data());
      });
    });
  }

// Equipment
  List _alldonations_equipmnent = [];
  CollectionReference user_equipment =
      FirebaseFirestore.instance.collection('Equipment_Request');
  getData_equipment() async {
    var responsebody = await user_equipment.get();
    responsebody.docs.forEach((element) {
      setState(() {
        _alldonations_equipmnent.add(element.data());
      });
    });
  }

  // Printed
  List _alldonations_Printed = [];
  CollectionReference user_printed =
      FirebaseFirestore.instance.collection('Printed_Request');
  getData_printed() async {
    var responsebody = await user_printed.get();
    responsebody.docs.forEach((element) {
      setState(() {
        _alldonations_Printed.add(element.data());
      });
    });
  }

  // Suppplies
  List _alldonations_Suppplies = [];
  CollectionReference user_suppplies =
      FirebaseFirestore.instance.collection('Supplies_Request');
  getData_supplies() async {
    var responsebody = await user_suppplies.get();
    responsebody.docs.forEach((element) {
      setState(() {
        _alldonations_Suppplies.add(element.data());
      });
    });
  }

  Future<void> uploadData() async {
    await getData_clothes();
    await getData_equipment();
    await getData_printed();
    await getData_supplies();
  }

  @override
  void initState() {
    uploadData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var newList = [
      _alldonations_clothes,
      _alldonations_equipmnent,
      _alldonations_Printed,
      _alldonations_Suppplies
    ].expand((x) => x).toList();

    return GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.grey[100],
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.black),
              leading: BackButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => bottomMenu(),
                    ),
                  );
                },
              ),
              backgroundColor: Colors.grey[100],
              elevation: 0.0,
              title: Text(
                'Return to the previous page',
                style: TextStyle(
                    fontFamily: 'Roboto',
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
            body: SingleChildScrollView(
                child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Current Requests',
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 35,
                              color: Color(0XFF2f4858),
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Clothes',
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 25,
                              color: Color(0xFF86BBD8),
                              fontWeight: FontWeight.bold),
                        ),
                        Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          margin: EdgeInsets.all(5),
                          elevation: 10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: clothes_option(_alldonations_clothes),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Equipment Materials',
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 25,
                              color: Color(0xFF86BBD8),
                              fontWeight: FontWeight.bold),
                        ),
                        Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          margin: EdgeInsets.all(5),
                          elevation: 10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: clothes_option(_alldonations_equipmnent),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Printed Materials',
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 25,
                              color: Color(0xFF86BBD8),
                              fontWeight: FontWeight.bold),
                        ),
                        Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          margin: EdgeInsets.all(5),
                          elevation: 10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: printed_option(_alldonations_Printed),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Supplies Materials',
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 25,
                              color: Color(0xFF86BBD8),
                              fontWeight: FontWeight.bold),
                        ),
                        Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          margin: EdgeInsets.all(5),
                          elevation: 10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: printed_option(_alldonations_Suppplies),
                          ),
                        )
                      ],
                    )))));
  }
}
