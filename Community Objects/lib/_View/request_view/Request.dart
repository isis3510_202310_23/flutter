import 'dart:io';
import 'dart:math';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import '../../view/bar.dart';
import '../optionClases.dart';
import '../../_Model/request_model/Organize_data_to_DB_request.dart';
import '../../_Blocs/product_option.dart';

class RequestPage extends StatelessWidget {
  const RequestPage({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: const MyRequestPage(),
    );
  }
}

class MyRequestPage extends StatefulWidget {
  const MyRequestPage({Key? key}) : super(key: key);

  @override
  State<MyRequestPage> createState() => _MyRequestPageState();
}

List<String> categories = [
  'Clothes',
  'Books and Printed Materials',
  'Protective equipment',
  'School and university supplies',
];

class _MyRequestPageState extends State<MyRequestPage> {
  List<String?> objetos = [];
  String? dropdownvalue = 'Clothes';

  optionClases opt = optionClases(1);
  final _formKey = GlobalKey<FormState>();

  // Show dialogs
  Future<dynamic> dialog(String message, String? dropdownvalue, int centinela) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            title: Text(message),
            titleTextStyle: TextStyle(
                color: Colors.black, fontSize: 16, fontFamily: 'Hind'),
            actions: <Widget>[
              new ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Color(0xFF86BBD8)),
                  onPressed: () {
                    if (centinela == 0) {
                      delete_content_categories(dropdownvalue);
                    }

                    Navigator.of(context).pop();
                  },
                  child: new Text("Done")),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          resizeToAvoidBottomInset: true,
          backgroundColor: Color(0XFF2f4858),
          appBar: AppBar(
            iconTheme: IconThemeData(color: Colors.black),
            leading: BackButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => bottomMenu(),
                  ),
                );
              },
            ),
            backgroundColor: Colors.grey[100],
            elevation: 0.0,
            title: Text(
              'Return to the previous page',
              style: TextStyle(
                  fontFamily: 'Roboto',
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 30, 20, 0),
              child: Form(
                  key: _formKey,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                            child: Text(
                          "Make a Request",
                          style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        )),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          'Category',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                        DropdownButton2(
                          value: dropdownvalue,
                          style: TextStyle(color: Colors.white, fontSize: 20),
                          isExpanded: true,
                          items: categories
                              .map((_category) => DropdownMenuItem<String>(
                                  value: _category, child: Text(_category)))
                              .toList(),
                          onChanged: (_category) {
                            setState(() {
                              dropdownvalue = _category!;
                            });
                          },
                          dropdownStyleData: DropdownStyleData(
                              decoration: BoxDecoration(
                                  color: Color(0XBF2f4858),
                                  borderRadius: BorderRadius.circular(20))),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        opt = options_categories(dropdownvalue),
                        Row(
                          children: [
                            Expanded(
                                child: ElevatedButton(
                                    onPressed: () {
                                      objetos = opt.objects();
                                      if (_formKey.currentState!.validate()) {
                                        objetos = opt.objects();

                                        if ((objetos[0] != '' &&
                                                objetos[1] != '' &&
                                                objetos[2] != '' &&
                                                objetos[3] != '') ||
                                            (objetos[0] != null &&
                                                objetos[1] != null &&
                                                objetos[2] != null &&
                                                objetos[3] != null)) {
                                          function_DB_request(
                                              dropdownvalue, objetos);
                                          dialog(
                                              'Your request has been successfully uploaded',
                                              dropdownvalue,
                                              0);
                                        } else {
                                          dialog('Please correct the errors',
                                              "", 1);
                                        }
                                      }
                                      ;
                                    },
                                    child: Text('Register'),
                                    style: ElevatedButton.styleFrom(
                                      primary: Color(0xFFF28919),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0)),
                                    ))),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                                child: ElevatedButton(
                                    onPressed: () {},
                                    child: Text('Clear'),
                                    style: ElevatedButton.styleFrom(
                                      primary: Color(0xFFF28919),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0)),
                                    ))),
                          ],
                        )
                      ])),
            ),
          )),
    );
  }
}
