import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import '../../_Blocs/contactUs_bloc/contact_bloc.dart';
import '../../view/bar.dart';

String email = Uri.encodeComponent("community@uniandes.edu.co");

// Page Support
String subject_1 = Uri.encodeComponent("Page Support");
String body_1 = Uri.encodeComponent(
    "Hi! Please let us know in which feature or view you experimented the issue.");
Uri mail_1 = Uri.parse("mailto:$email?subject=$subject_1&body=$body_1");

// Give your opinion
String subject_2 = Uri.encodeComponent("Give your opinion");
String body_2 = Uri.encodeComponent(
    "Let us know what do you think of the app and how we can improve it");
Uri mail_2 = Uri.parse("mailto:$email?subject=$subject_2&body=$body_2");

// Share your ideas
String subject_3 = Uri.encodeComponent("Share your ideas");
String body_3 = Uri.encodeComponent(
    "Do you want another category? \nDo you want another description of the objets?");
Uri mail_3 = Uri.parse("mailto:$email?subject=$subject_3&body=$body_3");

class ContactUsPage extends StatelessWidget {
  const ContactUsPage({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: const ContactUsPage_(),
    );
  }
}

class ContactUsPage_ extends StatefulWidget {
  const ContactUsPage_({Key? key}) : super(key: key);

  @override
  State<ContactUsPage_> createState() => _ContactUsPage();
}

class _ContactUsPage extends State<ContactUsPage_> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[100],
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          leading: BackButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => bottomMenu(),
                ),
              );
            },
          ),
          backgroundColor: Colors.grey[100],
          elevation: 0.0,
          title: Text(
            'Return to the previous page',
            style: TextStyle(
                fontFamily: 'Roboto',
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
        ),
        body: SafeArea(
            child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Customer Support ',
                  style: TextStyle(
                    fontFamily: 'Hind_2',
                    letterSpacing: 1,
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFFF28919),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Please, feel free to contact us at any time at the following email address',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontFamily: 'Hind',
                    letterSpacing: 1,
                    fontSize: 20,
                  ),
                ),
                Row(
                  children: [
                    SizedBox(height: 40),
                    ElevatedButton(
                      onPressed: () {
                        message(1, mail_1, context);
                      },
                      child: Icon(
                        Icons.email,
                        color: Color(0xFF86BBD8),
                      ),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.transparent,
                        foregroundColor: Colors.black,
                        elevation: 0,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Page Support',
                      style: TextStyle(
                        fontFamily: 'Hind',
                        letterSpacing: 1,
                        fontSize: 18,
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: [
                    SizedBox(height: 40),
                    ElevatedButton(
                      onPressed: () {
                        message(2, mail_2, context);
                        ;
                      },
                      child: Icon(
                        Icons.email,
                        color: Color(0xFF86BBD8),
                      ),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.transparent,
                        foregroundColor: Colors.black,
                        elevation: 0,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Give Your Opinion',
                      style: TextStyle(
                        fontFamily: 'Hind',
                        letterSpacing: 1,
                        fontSize: 18,
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: [
                    SizedBox(height: 40),
                    ElevatedButton(
                      onPressed: () {
                        message(3, mail_3, context);
                      },
                      child: Icon(
                        Icons.email,
                        color: Color(0xFF86BBD8),
                      ),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.transparent,
                        foregroundColor: Colors.black,
                        elevation: 0,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Share your ideas with us',
                      style: TextStyle(
                        fontFamily: 'Hind',
                        letterSpacing: 1,
                        fontSize: 18,
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        )));
  } // Root widget[clases]
}
