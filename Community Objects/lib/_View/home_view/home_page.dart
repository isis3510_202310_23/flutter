// ignore_for_file: prefer_const_constructors
import 'dart:async';
import 'package:app/_View/home_view/item_carousel.dart';
import 'package:app/widgets/menu_bar.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import '../../_Blocs/home_bloc/home_bloc.dart';
import 'package:flutter/material.dart';
import '../../widgets/categoria_card.dart';
import '../catalogue_view/catalogue.dart';
import '../../_Blocs/connection_bloc/connection_bloc.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();

}

class _HomePageState extends State<HomePage> {
  late Future futureClothes;
  late Future futureEquipment;
  late Future futurePrints;
  late Future futureSupplies;

  void _actualizarCategoria(String id) {
    String categoria = HomeBloc().actualizarCategoria(id);
    print(HomeBloc().categoriasOrdenadas.first.totalSeleccionado);
    setState(() {});

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => FutureBuilder(
                future: HomeBloc().loadCategory(categoria),
                builder: ((context, snapshot) {
                  if (snapshot.hasData) {
                    return Catalogue(
                        categoryTitle: categoria, items: snapshot.data);
                  } else {
                    return CircularProgressIndicator();
                  }
                }))));
  }

  @override
  void initState() {
    super.initState();

    HomeBloc().setTokenSubscriptionStream();
    futureClothes = HomeBloc().loadCategory("Clothes");
    futureEquipment = HomeBloc().loadCategory("Equipment");
    futurePrints = HomeBloc().loadCategory("Printed");
    futureSupplies = HomeBloc().loadCategory("Supplies");
    HomeBloc().getPopularities();
  }

    final ConnectionBloc bloc = ConnectionBloc();

  void _showConnectionLostDialog(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Connection Lost'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Your internet connection has been lost. This app requires a stable internet connection to function properly.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }


  @override
  Widget build(BuildContext context) {
  return StreamBuilder<ConnectivityResult>(
    stream: bloc.connectionStatus,
    builder: (BuildContext context, AsyncSnapshot<ConnectivityResult> snapshot) {
      if (snapshot.hasData && snapshot.data == ConnectivityResult.none) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          _showConnectionLostDialog(context);
        });
      }
    return Scaffold(
        body: Container(
          decoration: BoxDecoration(color: Colors.grey[100]),
          child: SafeArea(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const SizedBox(height: 15),
                    const Text(
                      'Category',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 35,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ),
                    const SizedBox(height: 25),

                    // CATEGORIAS

                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // El elemento más clickeado
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.4,
                          height: MediaQuery.of(context).size.width * 0.47,
                          child: CategoriaCard(
                            categoria: HomeBloc().categoriasOrdenadas[0],
                            onTap: _actualizarCategoria,
                            isTopCategory: true,
                            isMostSelected: true,
                          ),
                        ),
                        const SizedBox(width: 8),
                        // La matriz 2x2 de las categorías menos clickeadas
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              SizedBox(
                                height:
                                    MediaQuery.of(context).size.width * 0.225,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: CategoriaCard(
                                        categoria:
                                            HomeBloc().categoriasOrdenadas[0],
                                        onTap: _actualizarCategoria,
                                        isTopCategory: true,
                                      ),
                                    ),
                                    const SizedBox(width: 8),
                                    Expanded(
                                      child: CategoriaCard(
                                        categoria:
                                            HomeBloc().categoriasOrdenadas[1],
                                        onTap: _actualizarCategoria,
                                        isTopCategory: true,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(height: 13),
                              SizedBox(
                                height:
                                    MediaQuery.of(context).size.width * 0.225,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: CategoriaCard(
                                        categoria:
                                            HomeBloc().categoriasOrdenadas[2],
                                        onTap: _actualizarCategoria,
                                        isBottomCategory: true,
                                      ),
                                    ),
                                    const SizedBox(width: 8),
                                    Expanded(
                                      child: CategoriaCard(
                                        categoria:
                                            HomeBloc().categoriasOrdenadas[3],
                                        onTap: _actualizarCategoria,
                                        isBottomCategory: true,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),

                    // CARRUSELES DE PRODUCTOS

                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 25),
                        Text(
                          'Products',
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 35,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                          textAlign: TextAlign.start,
                        ),
                        FutureBuilder(
                            future: futureClothes,
                            builder: ((context, snapshot) {
                              if (snapshot.hasData) {
                                return ItemCarousel(
                                  categoryTitle: 'Clothes',
                                  items: snapshot.data,
                                );
                              } else {
                                return CircularProgressIndicator();
                              }
                            })),
                        SizedBox(height: 10),
                        FutureBuilder(
                            future: futureEquipment,
                            builder: ((context, snapshot) {
                              if (snapshot.hasData) {
                                return ItemCarousel(
                                  categoryTitle: 'Protective Equipment',
                                  items: snapshot.data,
                                );
                              } else {
                                return CircularProgressIndicator();
                              }
                            })),
                        SizedBox(height: 10),
                        FutureBuilder(
                            future: futurePrints,
                            builder: ((context, snapshot) {
                              if (snapshot.hasData) {
                                return ItemCarousel(
                                  categoryTitle: 'Books & Printed Materials',
                                  items: snapshot.data,
                                );
                              } else {
                                return CircularProgressIndicator();
                              }
                            })),
                        SizedBox(height: 10),
                        FutureBuilder(
                            future: futureSupplies,
                            builder: ((context, snapshot) {
                              if (snapshot.hasData) {
                                return ItemCarousel(
                                  categoryTitle: 'School & University Supplies',
                                  items: snapshot.data,
                                );
                              } else {
                                return CircularProgressIndicator();
                              }
                            })),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        // MENU BAR
        drawer: COMenu());
  }
      );
  }
}
