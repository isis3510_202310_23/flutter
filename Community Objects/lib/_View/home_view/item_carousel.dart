import 'package:app/_View/catalogue_view/catalogue.dart';
import 'package:app/_View/detail_view/object_detail.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../../_Model/models/item.dart';

class ItemCarousel extends StatelessWidget {
  final String categoryTitle;
  final List<Item> items;

  const ItemCarousel({
    Key? key,
    required this.categoryTitle,
    required this.items,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int maxItems = 5;
    int itemsToShow = items.length >= maxItems ? maxItems : items.length;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          categoryTitle,
          style:
              TextStyle(fontFamily: 'Hind', fontSize: 24, color: Colors.black),
          textAlign: TextAlign.start,
        ),
        SizedBox(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(itemsToShow + 1, (index) {
                if (index == itemsToShow) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Catalogue(
                                    categoryTitle: categoryTitle,
                                    items: items)));
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.3,
                          height: MediaQuery.of(context).size.width * 0.3,
                          color: Colors.grey[200],
                          child: Icon(
                            Icons.add,
                            size: MediaQuery.of(context).size.width * 0.2,
                            color: Colors.grey[800],
                          ),
                        ),
                      ),
                    ),
                  );
                } else {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ObjectDetail(
                                    itemId: items[index].id,
                                    categoryName: items[index].category,
                                  )));
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              decoration: BoxDecoration(
                                color: Color(0xff86bbd8),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10)),
                                      child: CachedNetworkImage(
                                        imageUrl: items[index].imageURL,
                                        placeholder: (context, url) =>
                                            CircularProgressIndicator(),
                                        errorWidget: (context, url, error) =>
                                            Icon(Icons.error),
                                        width: MediaQuery.of(context).size.width *
                                            0.3,
                                        height:
                                            MediaQuery.of(context).size.width *
                                                0.3,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    Text(
                                      items[index].name,
                                      style: TextStyle(
                                          fontFamily: 'Hind_2',
                                          fontSize: 14,
                                          color: Colors.white),
                                      textAlign: TextAlign.right,
                                    ),
                                  ])),
                          SizedBox(height: 5),
                        ],
                      ),
                    ),
                  );
                }
              }),
            ),
          ),
        ),
      ],
    );
  }
}
