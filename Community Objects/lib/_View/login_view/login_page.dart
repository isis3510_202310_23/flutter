import 'package:app/_View/custom_widgets/my_arc.dart';

import '../../_Model/user_model/user_repositrory.dart';
import '../custom_widgets/my_button.dart';
import '../custom_widgets/my_textfield.dart';
import 'forgot_password.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  final Function()? onTap;

  const LoginPage({super.key, required this.onTap});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  // controllers
  final userController = TextEditingController();

  final passwordController = TextEditingController();

  //Repository
  final UserRepository userRepository = UserRepositoryImpl();

  // methods
  void signUserIn() async {
    // Loading Circle
    /*showDialog(
        context: context,
        builder: (context) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }); */

    String message = await userRepository.loginUser(
        userController.text, passwordController.text);

    if (message != "Success") {
      showErrorMessage(message);
    }

    // Remove loading circle
    //Navigator.pop(context);
  }

  // Wrong User Method
  void showErrorMessage(String message) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(message),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0XFF2f4858),
        body: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MyArc(
                      diameter: MediaQuery.of(context).size.width,
                      inside: Image.asset(
                        'lib/images/Logo.jpg',
                      ),
                    ),

                    const SizedBox(height: 50),

                    // username textfield
                    MyTextField(
                      controller: userController,
                      hintText: "Username",
                      obscureText: false,
                      preIcon: const Icon(Icons.account_circle),
                    ),

                    const SizedBox(height: 15),

                    // password textfield
                    MyTextField(
                      controller: passwordController,
                      hintText: "Password",
                      obscureText: true,
                      preIcon: const Icon(Icons.lock),
                    ),

                    const SizedBox(height: 20),

                    // signin button
                    MyButton(
                      onTap: signUserIn,
                      text: "LOGIN",
                    ),

                    const SizedBox(height: 25),

                    // Register now!
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (context) =>
                                    const ForgotPasswordPage());
                          },
                          child: const Text(
                            'Forgot Password?',
                            style: TextStyle(
                              fontFamily: 'Microsoft-JengHei',
                              fontSize: 20,
                              color: Color(0xff86BBD8),
                            ),
                          ),
                        ),
                        const SizedBox(width: 80),
                        GestureDetector(
                          onTap: widget.onTap,
                          child: const Text('Sign Up',
                              style: TextStyle(
                                  fontFamily: 'Microsoft-JengHei',
                                  fontSize: 20,
                                  color: Color(0xff86BBD8))),
                        ),
                      ],
                    )
                  ]),
            ),
          ),
        ));
  }
}
