import 'package:app/_Model/services/user_service.dart';

import '../../_Model/user_model/user_repositrory.dart';
import '../custom_widgets/my_button.dart';
import 'package:flutter/material.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({super.key});

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  // controllers
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  //Repository
  final UserRepository userRepository = UserRepositoryImpl();

  // methods
  void passwordReset() async {
    // Attempt to reset the password
    String message = await UserServices().resetPassword(emailController.text);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      title: Container(
        decoration: const BoxDecoration(
            color: Color(0xff86BBD8),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        child: Container(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: const [
                Icon(
                  Icons.lock_reset,
                  color: Colors.white,
                ),
                SizedBox(width: 4),
                Text(
                  "Forgot Password",
                  style: TextStyle(color: Colors.white),
                ),
              ],
            )),
      ),
      titlePadding: const EdgeInsets.all(0),
      content: Stack(
        children: <Widget>[
          Form(
            key: formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const Text(
                  "Enter your email address:",
                  style: TextStyle(color: Color(0xff86BBD8)),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(controller: emailController),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: MyButton(
                    text: "Reset",
                    onTap: passwordReset,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
