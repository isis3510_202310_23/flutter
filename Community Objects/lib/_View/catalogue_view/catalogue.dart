import 'package:flutter/material.dart';
import '../../_Model/models/item.dart';
import '../detail_view/object_detail.dart';
import 'package:cached_network_image/cached_network_image.dart';


class Catalogue extends StatefulWidget {
  final String categoryTitle;
  final List<Item> items;

  const Catalogue(
      {super.key, required this.categoryTitle, required this.items});

  @override
  State<Catalogue> createState() => _CatalogueState();
}

class _CatalogueState extends State<Catalogue> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int maxItems = 20;
    int itemsToShow =
        widget.items.length >= maxItems ? maxItems : widget.items.length;

    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: GridView(
        padding: const EdgeInsets.all(10),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, mainAxisSpacing: 20, crossAxisSpacing: 20),
        children: List.generate(
            itemsToShow,
            (index) => GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ObjectDetail(
                                  itemId: widget.items[index].id,
                                  categoryName: widget.items[index].category,
                                )));
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            decoration: BoxDecoration(
                              color: const Color(0xff86bbd8),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  ClipRRect(
                                    borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        topRight: Radius.circular(10)),
                                    child: CachedNetworkImage(
                                      imageUrl: widget.items[index].imageURL,
                                      width: MediaQuery.of(context).size.width *
                                          0.3,
                                      height:
                                          MediaQuery.of(context).size.width *
                                              0.3,
                                      fit: BoxFit.cover,
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                    ),
                                  ),
                                  Text(
                                    widget.items[index].name,
                                    style: const TextStyle(
                                        fontFamily: 'Hind_2',
                                        fontSize: 14,
                                        color: Colors.white),
                                    textAlign: TextAlign.right,
                                  ),
                                ])),
                        const SizedBox(height: 5),
                      ],
                    ),
                  ),
                )),
      )),
    );
  }
}
