import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import '../../_Blocs/detail_bloc/detail_bloc.dart';

class ObjectDetail extends StatefulWidget {
  const ObjectDetail(
      {super.key, required this.itemId, required this.categoryName});

  final String itemId;
  final String categoryName;

  @override
  State<ObjectDetail> createState() => _ObjectDetailState();
}

class _ObjectDetailState extends State<ObjectDetail> {
  late Future<DocumentSnapshot> futureItem;

  @override
  void initState() {
    super.initState();

    futureItem = DetailBloc().loadItem(widget.itemId, widget.categoryName);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: futureItem,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              body: Padding(
                padding: const EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 0.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                      child: ClipOval(
                        child: CachedNetworkImage(
                          imageUrl: DetailBloc().getItem().imageURL,
                          height: 200,
                          width: 200,
                          fit: BoxFit.cover,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      ),
                    ),
                    // Name of the product
                    const Text(
                      'Name: ',
                      style: TextStyle(
                          letterSpacing: 1,
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                    ),

                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      DetailBloc().getItem().name,
                      style: const TextStyle(
                        letterSpacing: 1,
                        fontSize: 20,
                      ),
                    ),

                    const SizedBox(height: 20),

                    const Text(
                      'Description: ',
                      style: TextStyle(
                          letterSpacing: 1,
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      DetailBloc().getItem().description,
                      style: const TextStyle(
                        letterSpacing: 1,
                        fontSize: 20,
                      ),
                    ),

                    const SizedBox(height: 20),

                    const Text(
                      'User: ',
                      style: TextStyle(
                          letterSpacing: 1,
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      DetailBloc().getItem().user,
                      style: const TextStyle(
                        letterSpacing: 1,
                        fontSize: 20,
                      ),
                    ),

                    const SizedBox(height: 20),

                    Column(
                        children: List.generate(
                      DetailBloc().getExtras().length,
                      (index) => Column(
                        children: [
                          Text(
                            "${DetailBloc().getExtras()[index].key}:",
                            style: const TextStyle(
                                letterSpacing: 1,
                                fontSize: 25,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            DetailBloc().getExtras()[index].value.toString(),
                            style: const TextStyle(
                              letterSpacing: 1,
                              fontSize: 20,
                            ),
                          ),
                          const SizedBox(height: 20),
                        ],
                      ),
                    ))
                  ],
                ),
              ),
            );
          } else {
            return CircularProgressIndicator();
          }
        });
  }
}
