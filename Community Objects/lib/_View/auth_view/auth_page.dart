import 'package:app/view/bar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'login_or_register.dart';

class AuthPage extends StatelessWidget {
  const AuthPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: StreamBuilder<User?>(
      stream: FirebaseAuth.instance.authStateChanges(),
      builder: (context, snapshot) {
        // Is the user logged in?
        if (snapshot.hasData) {
          return bottomMenu();
        }
        // Is the user NOT logged in?
        else {
          return const LoginOrRegisterPage();
        }
      },
    ));
  }
}
