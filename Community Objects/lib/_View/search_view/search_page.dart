import 'package:app/_Model/services/item_service.dart';
import 'package:app/_Model/services/search_service.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../catalogue_view/catalogue.dart';

class SearchView extends StatefulWidget {
  @override
  _SearchViewState createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  late TextEditingController searchController;
  late List<String> recentSearches;
  late List<String> mostSearched;
  late bool hasConnection;

  @override
  void initState() {
    super.initState();
    searchController = TextEditingController();
    recentSearches = [];
    mostSearched = [];
    hasConnection = true;

    // Load recent searches and most searched items from SharedPreferences / Firestore
    loadRecentSearches();
    loadMostSearched();
  }

  @override
  void dispose() {
    searchController.dispose();
    super.dispose();
  }

  Future<void> checkConnection() async {
    hasConnection = await InternetConnectionChecker().hasConnection;
    if (!hasConnection) {
      dialog('Please Check your internet connection, this feature needs it',
          context);
    }
  }

  Future<dynamic> dialog(String message, BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            title: Text(message),
            titleTextStyle: TextStyle(
                color: Colors.black, fontSize: 16, fontFamily: 'Hind'),
            actions: <Widget>[
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Color(0xFF86BBD8)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Done")),
            ],
          );
        });
  }

  Future<void> loadRecentSearches() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      recentSearches = prefs.getStringList('recentSearches') ?? [];
    });
  }

  Future<void> loadMostSearched() async {
    await checkConnection();
    if (!hasConnection) {
      return;
    }
    final snapshot = await FirebaseFirestore.instance
        .collection('searches')
        .orderBy('count', descending: true)
        .limit(5)
        .get();
    final mostSearchedQuery = snapshot.docs.map((doc) => doc.id).toList();
    setState(() {
      mostSearched = mostSearchedQuery;
    });
  }

  void saveRecentSearch(String query) async {
    final prefs = await SharedPreferences.getInstance();
    final updatedSearches = List<String>.from(recentSearches);
    updatedSearches.remove(query);
    updatedSearches.insert(0, query);
    if (updatedSearches.length > 5) {
      updatedSearches.removeLast();
    }
    await prefs.setStringList('recentSearches', updatedSearches);
    setState(() {
      recentSearches = updatedSearches;
    });
  }

  void onSearchSubmitted(String query) async {
    saveRecentSearch(query);

    checkConnection();
    if (!hasConnection) {
      return;
    }

    SearchServices().updateSearchTerm(query);

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => FutureBuilder(
                future: ItemServices().getItems(query),
                builder: ((context, snapshot) {
                  if (snapshot.hasData) {
                    return Catalogue(
                        categoryTitle: "Search Result", items: snapshot.data!);
                  } else {
                    return CircularProgressIndicator();
                  }
                }))));
  }

  void clearRecentSearches() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('recentSearches');
    setState(() {
      recentSearches = [];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        backgroundColor: Colors.grey[100],
        elevation: 0.0,
        title: const Text(
          'Search',
          style: TextStyle(
              letterSpacing: 1,
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Colors.black),
        ),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: searchController,
              onSubmitted: onSearchSubmitted,
              decoration: InputDecoration(
                hintText: 'Search',
                suffixIcon: IconButton(
                  icon: const Icon(Icons.clear),
                  onPressed: () {
                    searchController.clear();
                  },
                ),
              ),
            ),
          ),
          ListTile(
            title: const Text(
              'Recent Searches',
              style: TextStyle(
                  letterSpacing: 1,
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
            trailing: IconButton(
              icon: const Icon(Icons.clear_all),
              onPressed: clearRecentSearches,
            ),
          ),
          ListView.builder(
            shrinkWrap: true,
            itemCount: recentSearches.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(recentSearches[index]),
                onTap: () {
                  onSearchSubmitted(recentSearches[index]);
                },
              );
            },
          ),
          const ListTile(
            title: Text(
              'Most Searched',
              style: TextStyle(
                  letterSpacing: 1,
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          ListView.builder(
            shrinkWrap: true,
            itemCount: mostSearched.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(mostSearched[index]),
                onTap: () {
                  onSearchSubmitted(mostSearched[index]);
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
