import 'package:flutter/material.dart';

import 'my_painter.dart';

class MyArc extends StatelessWidget {
  final double diameter;
  final Widget inside;

  const MyArc({super.key, this.diameter = 200, required this.inside});

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
        painter: MyPainter(),
        size: Size(diameter, diameter),
        child: SizedBox(height: diameter, width: diameter, child: inside));
  }
}
