
import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final Function()? onTap;
  final String text;

  const MyButton({super.key, required this.onTap, required this.text});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 15),
          margin: const EdgeInsets.symmetric(horizontal: 25),
          decoration: BoxDecoration(
            color: const Color(0xff86BBD8),
            borderRadius: BorderRadius.circular(40),
          ),
          child: Center(
            child: Text(text,
                style: const TextStyle(
                  fontFamily: 'Microsoft-JengHei',
                  fontSize: 20,
                  color: Colors.white,
                )),
          ),
        ));
  }
}
