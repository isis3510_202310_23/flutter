// ignore_for_file: prefer_const_constructors

import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:light/light.dart';
import 'package:provider/provider.dart';
import '../../view/bar.dart';
import '../optionClases.dart';
import '../../_Model/donate_model/Organize_data_to_DB_donate.dart';
import '../../_Blocs/product_option.dart';

class DonatePage extends StatelessWidget {
  const DonatePage({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: const MyDonatePage(),
    );
  }
}

class MyDonatePage extends StatefulWidget {
  const MyDonatePage({Key? key}) : super(key: key);

  @override
  State<MyDonatePage> createState() => _MyDonatePageState();
}

List<String> categories = [
  'Clothes',
  'Books and Printed Materials',
  'Protective equipment',
  'School and university supplies',
];

class _MyDonatePageState extends State<MyDonatePage> {
  // Variables
  File? image;
  List<String?> objetos = [];
  String? dropdownvalue = 'Clothes';
  String? path_;

  bool dimlyLit = false;
  late Light _light;
  late StreamSubscription _subscription;

  optionClases opt = optionClases(1);
  final _formKey = GlobalKey<FormState>();

  // Light
  void onData(int luxValue) async {
    if (luxValue < 15 && !dimlyLit) {
      switchLitLevel(true);
      stopListening();
    } else if (luxValue > 15 && dimlyLit) {
      switchLitLevel(false);
    }
  }

  void switchLitLevel(bool choice) {
    setState(() {
      dimlyLit = choice;
    });

    if (dimlyLit) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            icon: Icon(Icons.light),
            iconColor: Colors.red,
            title: Text(
                "We recommend taking a picture under better lighting conditions."),
          );
        },
      );
    }
  }

  void stopListening() {
    _subscription.cancel();
  }

  void startListening() {
    _light = Light();
    try {
      _subscription = _light.lightSensorStream.listen(onData);
    } on LightException catch (exception) {
      print(exception);
    }
  }

  Future<void> initPlatformState() async {
    startListening();
  }

  // Import image from gallery
  Future pickImage() async {
    try {
      stopListening();
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);

      if (image == null) return;

      final imageTemp = File(image.path);
      path_ = image.path;

      setState(() => this.image = imageTemp);
      startListening();
    } on PlatformException catch (e) {
      print('Failed to pick image: $e');
    }
  }

  // Take a picture
  Future pickImageC() async {
    try {
      stopListening();
      final image = await ImagePicker().pickImage(source: ImageSource.camera);

      if (image == null) return;

      final imageTemp = File(image.path);
      path_ = image.path;

      setState(() => this.image = imageTemp);
      startListening();
    } on PlatformException catch (e) {
      print('Failed to pick image: $e');
    }
  }

  // Show dialogs
  Future<dynamic> dialog(String message, String? dropdownvalue, int centinela) {
    return Future.delayed(Duration.zero, () {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              title: Text(message),
              titleTextStyle: TextStyle(
                  color: Colors.black, fontSize: 16, fontFamily: 'Hind'),
              actions: <Widget>[
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Color(0xFF86BBD8)),
                    onPressed: () {
                      if (centinela == 0) {
                        delete_content_categories(dropdownvalue);
                      }

                      Navigator.of(context).pop();
                    },
                    child: Text("Done")),
              ],
            );
          });
    });
  }

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  @override
  Widget build(BuildContext context) {
    //delete_content_categories(dropdownvalue);
    return GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.grey[100],
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.black),
              leading: BackButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => bottomMenu(),
                    ),
                  );
                },
              ),
              backgroundColor: Colors.grey[100],
              elevation: 0.0,
              title: Text(
                'Return to the previous page',
                style: TextStyle(
                    fontFamily: 'Roboto',
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(top: 20, bottom: 0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20)),
                          height: 270,
                          margin: const EdgeInsets.only(
                              top: 10, left: 10, right: 10, bottom: 20),
                          child: Column(children: [
                            Text('Make a Donation',
                                style: TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black)),
                            SizedBox(
                              height: 10,
                            ),
                            Center(
                                child: image != null
                                    ? Image.file(
                                        image!,
                                        scale: 0.1,
                                        height: 220,
                                      )
                                    : Text(
                                        "First Step: Select an image of your material",
                                        style: TextStyle(
                                            fontSize: 20, color: Colors.black),
                                        textAlign: TextAlign.center,
                                      ))
                          ])),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                          padding:
                              EdgeInsets.only(top: 10, left: 20, right: 20),
                          decoration: BoxDecoration(
                              color: Color(0XFF2f4858),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                              )),
                          width: double.maxFinite,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              SizedBox(
                                height: 5,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment
                                    .start, //Center Row contents horizontally,,
                                children: [
                                  ElevatedButton(
                                    onPressed: () {
                                      pickImage();
                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: Color(0xFF86BBD8),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0)),
                                    ),
                                    child: const Text("Pick Image from Gallery",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                  SizedBox(
                                    width: 50,
                                  ),
                                  ElevatedButton(
                                      onPressed: () {
                                        pickImageC();
                                      },
                                      style: ElevatedButton.styleFrom(
                                        backgroundColor: Color(0xFF86BBD8),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10.0)),
                                      ),
                                      child: const Text(
                                          "Pick Image from Camera",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold))),
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text("Category",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(
                                height: 5,
                              ),
                              DropdownButton2(
                                value: dropdownvalue,
                                isExpanded: true,
                                style: TextStyle(color: Colors.white),
                                items: categories
                                    .map((category) => DropdownMenuItem<String>(
                                        value: category, child: Text(category)))
                                    .toList(),
                                onChanged: (category) {
                                  setState(() {
                                    dropdownvalue = category!;
                                  });
                                },
                                dropdownStyleData: DropdownStyleData(
                                    decoration: BoxDecoration(
                                        color: Color(0XFF2f4858),
                                        borderRadius:
                                            BorderRadius.circular(20))),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              opt = options_categories(dropdownvalue),
                              Row(
                                children: [
                                  Expanded(
                                      child: ElevatedButton(
                                          onPressed: () {
                                            objetos = opt.objects();
                                            if (_formKey.currentState!
                                                .validate()) {
                                              objetos = opt.objects();
                                              if (path_ == null) {
                                                dialog(
                                                    'There is no photo summited',
                                                    "",
                                                    1);
                                              } else {
                                                if ((objetos[0] != '' &&
                                                        objetos[1] != '' &&
                                                        objetos[2] != '' &&
                                                        objetos[3] != '') ||
                                                    (objetos[0] != null &&
                                                        objetos[1] != null &&
                                                        objetos[2] != null &&
                                                        objetos[3] != null)) {
                                                  objetos[4] = path_;

                                                  function_DB(
                                                      dropdownvalue, objetos);
                                                  dialog(
                                                      'Your donation has been successfully uploaded',
                                                      dropdownvalue,
                                                      0);
                                                } else {
                                                  dialog(
                                                      'Please correct the errors',
                                                      "",
                                                      1);
                                                }
                                              }
                                            }
                                            ;
                                          },
                                          style: ElevatedButton.styleFrom(
                                            backgroundColor: Color(0xFFF28919),
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        10.0)),
                                          ),
                                          child: Text('Submit',
                                              style: TextStyle(
                                                  fontWeight:
                                                      FontWeight.bold)))),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                      child: ElevatedButton(
                                          onPressed: () {
                                            delete_content_categories(
                                                dropdownvalue);
                                            path_ = null;
                                            image = null;
                                          },
                                          style: ElevatedButton.styleFrom(
                                            backgroundColor: Color(0xFFF28919),
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        10.0)),
                                          ),
                                          child: Text(
                                            'Clear',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ))),
                                ],
                              )
                            ],
                          )),
                    ],
                  ),
                ),
              ),
            )));
  }
}
