import 'dart:developer';
import 'dart:ffi';
import 'package:app/_Model/rating/connection_database_rating.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'dart:ui';
import 'dart:ffi';
import '../../view/bar.dart';
import '../auth_view/auth_page.dart';

double? rate_donate = null;
double? rate_request = null;
double? rate_app = null;

class RatePage extends StatelessWidget {
  const RatePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: RatePage_());
  }
}

class RatePage_ extends StatefulWidget {
  const RatePage_({Key? key}) : super(key: key);

  @override
  State<RatePage_> createState() => _RatePage();
}

class _RatePage extends State<RatePage_> {
  void _signUserOut() {
    FirebaseAuth.instance.signOut();
  }

  List<double> calificaciones = [0, 0, 0];
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        backgroundColor: Colors.grey[100],
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          leading: BackButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => bottomMenu(),
                ),
              );
            },
          ),
          backgroundColor: Colors.grey[100],
          elevation: 0.0,
          title: Text(
            'Return to the previous page',
            style: TextStyle(
                fontFamily: 'Roboto',
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
              padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 0.0),
              child: Column(
                children: [
                  Text(
                    "We wanted to know how was your experience",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'How do you rate our Donate service?',
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontFamily: 'Hind',
                      letterSpacing: 1,
                      fontSize: 20,
                    ),
                  ),
                  Center(child: _ratingBar_D()),
                  Text(
                    rate_donate != null ? 'Rate: $rate_donate' : 'Rate it!',
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontFamily: 'Hind_2',
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'How do you rate our Request service?',
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontFamily: 'Hind',
                      letterSpacing: 1,
                      fontSize: 20,
                    ),
                  ),
                  Center(child: _ratingBar_R()),
                  Text(
                    rate_request != null ? 'Rate: $rate_request' : 'Rate it!',
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontFamily: 'Hind_2',
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'How do you rate our app?',
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontFamily: 'Hind',
                      letterSpacing: 1,
                      fontSize: 20,
                    ),
                  ),
                  Center(child: _ratingBar_I()),
                  Text(
                    rate_app != null ? 'Rate: $rate_app' : 'Rate it!',
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontFamily: 'Hind_2',
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    width: double.infinity,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Color(0xffF28919), // background
                        onPrimary: Colors.white, // foreground
                      ),
                      onPressed: () {
                        print(calificaciones);
                        add_rating(calificaciones[0], calificaciones[1],
                            calificaciones[2]);
                        _signUserOut();
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AuthPage(),
                          ),
                        );
                      },
                      child: Text(
                        'Sign Out',
                        style: TextStyle(
                            fontFamily: 'Hind',
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              )),
        ),
      ),
    );
  }

  Widget _ratingBar_D() {
    return RatingBar(
        initialRating: 0,
        minRating: 0,
        maxRating: 5,
        allowHalfRating: true,
        itemPadding: EdgeInsets.symmetric(horizontal: 7.0),
        ratingWidget: RatingWidget(
          full:
              const Icon(Icons.star, color: Color.fromARGB(255, 69, 170, 220)),
          half: const Icon(Icons.star_half,
              color: Color.fromARGB(255, 69, 170, 220)),
          empty: const Icon(Icons.star_border,
              color: Color.fromARGB(255, 69, 170, 220)),
        ),
        onRatingUpdate: (value) {
          setState(() {
            calificaciones[0] = value;
            rate_donate = value;
          });
        });
  }

  Widget _ratingBar_R() {
    return RatingBar(
        initialRating: 0,
        minRating: 0,
        maxRating: 5,
        allowHalfRating: true,
        itemPadding: EdgeInsets.symmetric(horizontal: 7.0),
        ratingWidget: RatingWidget(
          full:
              const Icon(Icons.star, color: Color.fromARGB(255, 69, 170, 220)),
          half: const Icon(Icons.star_half,
              color: Color.fromARGB(255, 69, 170, 220)),
          empty: const Icon(Icons.star_border,
              color: Color.fromARGB(255, 69, 170, 220)),
        ),
        onRatingUpdate: (value) {
          setState(() {
            calificaciones[1] = value;
            rate_request = value;
          });
        });
  }

  Widget _ratingBar_I() {
    return RatingBar(
        initialRating: 0,
        minRating: 0,
        maxRating: 5,
        allowHalfRating: true,
        itemPadding: EdgeInsets.symmetric(horizontal: 7.0),
        ratingWidget: RatingWidget(
          full:
              const Icon(Icons.star, color: Color.fromARGB(255, 69, 170, 220)),
          half: const Icon(Icons.star_half,
              color: Color.fromARGB(255, 69, 170, 220)),
          empty: const Icon(Icons.star_border,
              color: Color.fromARGB(255, 69, 170, 220)),
        ),
        onRatingUpdate: (value) {
          setState(() {
            calificaciones[2] = value;
            rate_app = value;
          });
        });
  }
}
