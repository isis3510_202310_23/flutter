// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyCbucM2nTDwKH4uRY6cSlGkqeprUx-Km0M',
    appId: '1:476636466129:web:3a863ef1f18f1488e1aef0',
    messagingSenderId: '476636466129',
    projectId: 'community-auth-e564a',
    authDomain: 'community-auth-e564a.firebaseapp.com',
    databaseURL: 'https://community-auth-e564a-default-rtdb.firebaseio.com',
    storageBucket: 'community-auth-e564a.appspot.com',
    measurementId: 'G-2CDWGGPXKJ',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyCNWg_HqGlWe4VBdyBR70BkNVvtdHJmxwk',
    appId: '1:476636466129:android:9a4e4e5f24a009f5e1aef0',
    messagingSenderId: '476636466129',
    projectId: 'community-auth-e564a',
    databaseURL: 'https://community-auth-e564a-default-rtdb.firebaseio.com',
    storageBucket: 'community-auth-e564a.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyCNPwqb3dmKyWAM0RO9GGaaXj8Pd5jiun4',
    appId: '1:476636466129:ios:d2fb0eca989fd40de1aef0',
    messagingSenderId: '476636466129',
    projectId: 'community-auth-e564a',
    databaseURL: 'https://community-auth-e564a-default-rtdb.firebaseio.com',
    storageBucket: 'community-auth-e564a.appspot.com',
    androidClientId: '476636466129-f1k3tpjutqad9qcm8rco61i3362cr9qr.apps.googleusercontent.com',
    iosClientId: '476636466129-osn51a1dto380h2s697ednh129umgop3.apps.googleusercontent.com',
    iosBundleId: 'com.example.communityObjects',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyCNPwqb3dmKyWAM0RO9GGaaXj8Pd5jiun4',
    appId: '1:476636466129:ios:d2fb0eca989fd40de1aef0',
    messagingSenderId: '476636466129',
    projectId: 'community-auth-e564a',
    databaseURL: 'https://community-auth-e564a-default-rtdb.firebaseio.com',
    storageBucket: 'community-auth-e564a.appspot.com',
    androidClientId: '476636466129-f1k3tpjutqad9qcm8rco61i3362cr9qr.apps.googleusercontent.com',
    iosClientId: '476636466129-osn51a1dto380h2s697ednh129umgop3.apps.googleusercontent.com',
    iosBundleId: 'com.example.communityObjects',
  );
}
